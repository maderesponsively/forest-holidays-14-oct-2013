﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Google Map and Location.xslt                                                   
    Created by: sitecore\paulh                                       
    Created: 26/09/2012 15:21:41                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- Includes -->
<xsl:include href="includes/Functions.xslt"/>

<!-- variables -->
<!--<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />-->

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<div id="mapAndLocation">
		<div id="googleMap">
			<xsl:value-of select="sc:fld('Google Map URL',.)" disable-output-escaping="yes"/>
		</div>
		<div id="address">
		<xsl:call-template name="br">
			<xsl:with-param name="text" select="sc:fld('Address',.)"/>
		</xsl:call-template>
			<xsl:if test="sc:fld('Public Transport Directions',.)">
				<br />
				<br />
				<h3>Public transport information</h3>
				<xsl:value-of select="sc:fld('Public Transport Directions',.)" disable-output-escaping="yes"/>
			</xsl:if>
		</div>
	</div>
	<!--<xsl:variable name="customVariable" select="fh:customMethod('someParameter')"></xsl:variable>-->
</xsl:template>

</xsl:stylesheet>