﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Head Tags.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 19/07/2012 15:18:28                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!-- Uncomment one of the following lines if you need a "home" variable in your code -->
<!--<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />-->
<!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
<!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->
<!--<xsl:variable name="siteSettings" select="sc:item('/sitecore/content/site settings',.)" />-->


<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<title><xsl:value-of select="sc:field('Page Title',.)" disable-output-escaping="yes" /></title>
	<xsl:text>&#10;</xsl:text>
	<meta name="keywords">
		<xsl:attribute name="content">
			<xsl:value-of select="sc:field('Keywords',.)" disable-output-escaping="yes"/>
		</xsl:attribute>
	</meta>
	<xsl:text>&#10;</xsl:text>
	<meta name="description">
		<xsl:attribute name="content">
			<xsl:value-of select="sc:field('Description',.)" disable-output-escaping="yes"/>
		</xsl:attribute>
	</meta>
</xsl:template>

</xsl:stylesheet>