﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Body Content.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 14/09/2012 10:43:41                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">

	<xsl:if test="sc:fld('Body Content',.)">
		<div class="bodyContent">
			<sc:html field="Body Content" />
		</div>
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>