﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Page Carousel.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 05/09/2012 11:33:12                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8" />

	<!-- parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>

	<!-- entry point -->
	<xsl:template match="*">
		<xsl:apply-templates select="$sc_item" mode="main"/>
	</xsl:template>

	<!--==============================================================-->
	<!-- main                                                         -->
	<!--==============================================================-->
	<xsl:template match="*" mode="main">

		<xsl:if test="sc:fld('Carousel Slides',.) and sc:fld('Show Carousel',.) = '1'">
			<div id="holder">
				<div id="slider">
					<div class="box">
						<div class="wrap wrap-carousel">
							<xsl:choose>
								<xsl:when test="sc:fld('Season Slider',.) = '1'">
									<!-- Season Slider Mode. Only show up to 4 slides -->
									<div id="seasons">
										<ul class="carousel">
											<xsl:for-each select="sc:Split('Carousel Slides',.)">
												<xsl:if test="position() &lt; 5">
													<xsl:for-each select="sc:item(text(),.)">
														<li>
															<sc:image field="Image" />
														</li>
													</xsl:for-each>
												</xsl:if>
											</xsl:for-each>
										</ul>
										<div class="controls">
											<div class="pages">
												<a href="#" id="icon-season-spring" class="active"></a>
												<a href="#" id="icon-season-summer"></a>
												<a href="#" id="icon-season-autumn"></a>
												<a href="#" id="icon-season-winter"></a>
											</div>
										</div>
										<div class="wrap-header">
											<h2><xsl:value-of select="sc:fld('Season Slider Header',.)"/></h2>
										</div>
									</div>
								</xsl:when>
								<xsl:otherwise>
									<div id="hero">
										<ul class="carousel">
											<!-- Normal Carousel Mode -->
											<xsl:for-each select="sc:Split('Carousel Slides',.)">
												<xsl:for-each select="sc:item(text(),.)">
													<li>
														<sc:image field="Image" />
														<sc:html field="HTML" />
													</li>
												</xsl:for-each>
											</xsl:for-each>
										</ul>
										<a href="#" class="carousel-prev">
											<span>prev</span>
										</a>
										<a href="#" class="carousel-next">
											<span>next</span>
										</a>
										<div class="controls">
											<div class="pages"></div>
										</div>
										<div id="shadow"></div>
									</div>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
				</div>
			</div>
			<!-- /#hero -->
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>