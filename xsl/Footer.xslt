﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Footer.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 13/09/2012 14:58:06                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />

	<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<footer>
    
    	<div id="desktop">
		<div class="top">
			<img src="/img/footer-top.png" alt="" />
		</div>
		<!-- /.top -->
		<div class="map">
			<sc:html field="Footer Map" select="$siteSettings" />
			<!-- /.mapArea -->
		</div>
		<!-- /.map -->
		<div style="float: right">
			<sc:html field="Footer Content" select="$siteSettings" />
		</div>
        </div>
        
        <div id="mobile" style="display:none;">
            <!--
                <section class="telephone">
                    <p><strong>Contact us</strong></p>
                    <p>Mon - Fri 08.30-17.30</p>
                    <p class="number">0845 130 8223</p>
                </section>
        
                <section class="email">
                    <p><strong>Email us</strong></p>
                    <p><a href="mailto:customerservice@forestholidays.co.uk">customerservice@forestholidays.co.uk</a></p>
                </section>    
                <section class="address">
                    <p>
                        <strong>Write to us</strong><br />
                        Forest Holidays<br />
                        Bath Yard<br />
                        Moira<br />
                        Derbyshire<br />
                        DE12 6BA<br />
                    </p>
                </section>
              -->
            <section class="links">
                
                <a class="btn Primary">
					<xsl:attribute name="href">
						<xsl:value-of select="sc:path(sc:item('{E07130FF-444E-468B-BE5E-170D0A3CD5D5}',.))" />
					</xsl:attribute>
					<span class="label">Order a brochure</span><img src="/img/btn-arrow-right.png" />
				</a>
                
                <ul class="linksgroup1">
					<xsl:for-each select="sc:Split('Header Navigation Items',$siteSettings)">
						<li>
							<sc:link select="sc:item(text(),.)" >
								<sc:text field="Navigation Link Text" select="sc:item(text(),.)" />
							</sc:link>
						</li>
					</xsl:for-each>
                </ul>
            
            </section>
            
        </div>
        
        
		<div class="bottom">
			<ul>
				<xsl:for-each select="sc:Split('Footer Social Media Links',$siteSettings)">
					<xsl:variable name="name" select="sc:fld('Site Name',sc:item(text(),.))" />
					<xsl:variable name="url" select="sc:fld('URL',sc:item(text(),.), 'url')" />
					<xsl:for-each select="sc:item(text(),.)">
						<li>
							<a href="{$url}">
								<img alt="{$name} logo">
									<xsl:attribute name="src">
										<xsl:value-of select="sc:fld('Image', ., 'src')" />
									</xsl:attribute>
								</img>
							</a>
						</li>
					</xsl:for-each>
				</xsl:for-each>
			</ul>
			<div class="links">
				<span class="copyright">&#169; Forest Holidays <xsl:value-of select="fh:CurrentYear()" /></span>
				<xsl:if test="sc:fld('Footer Links',$siteSettings)">
					<xsl:for-each select="sc:Split('Footer Links',$siteSettings)">
						<xsl:for-each select="sc:item(text(),.)">
							<sc:link >
								<sc:text field="Navigation Link Text" />
							</sc:link>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:if>
			</div>
			<!-- /.links -->
		</div>
		<!-- /.bottom -->
	</footer>
    
</xsl:template>

</xsl:stylesheet>