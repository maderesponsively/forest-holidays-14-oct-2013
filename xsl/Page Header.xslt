﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Page Header.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 03/09/2012 11:09:26                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8" />

	<!-- parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>

	<xsl:include href="includes/Header Navigation.xslt"/>

	<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<header class="layoutContainer">
		<a href="/">
			<div id="logo" title="Forest Holidays Home">
				<!--<img src="/img/logo.png" alt="Forest Holidays" />-->
			</div>
		</a>
		
		<!-- If we're in any of the 4 Booking Stages then show the Stage Indicator instead -->
		<xsl:variable name="bookingStage" select="fh:bookingStage($sc_currentitem/@id)"></xsl:variable>
		<xsl:choose>
			<xsl:when test="$bookingStage > 0">
				<div id="stageIndicator">
					<xsl:attribute name="class">
						<xsl:value-of select="concat('stage',$bookingStage)"/>
					</xsl:attribute>
					<!-- Labels -->
					<div id="stageIndicatorLabel1" class="stageIndicatorLabel">
						<sc:text select="$siteSettings" field="Booking Stage 1 Text" />
					</div>
					<div id="stageIndicatorLabel2" class="stageIndicatorLabel">
						<sc:text select="$siteSettings" field="Booking Stage 2 Text" />
					</div>
					<div id="stageIndicatorLabel3" class="stageIndicatorLabel">
						<sc:text select="$siteSettings" field="Booking Stage 3 Text" />
					</div>
					<div id="stageIndicatorLabel4" class="stageIndicatorLabel">
						<sc:text select="$siteSettings" field="Booking Stage 4 Text" />
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="renderHeaderNavigation" />
				<div id="buttons">
					<a href="/locations" id="explore">
						<span>Explore our UK locations</span>
						<img src="/img/explore-icon.png" alt="" />
					</a>

					<a href="#" id="availability">
						<span>Check prices &amp; availability</span>
						<img src="/img/availability-icon.png" alt="" />
					</a>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</header>
</xsl:template>

</xsl:stylesheet>