﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Cabin Details.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 08/10/2012 12:58:02                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- Includes -->
<xsl:include href="includes/Light Box Image.xslt"/>
<xsl:include href="includes/Light Box Video No Caption.xslt"/>
	
<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!--<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />-->

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<xsl:if test="sc:fld('Content Blocks',.)">
		<div id="contentBlockContainer">
			<xsl:for-each select="sc:Split('Content Blocks',.)">
				<xsl:variable name="counter" select="position()" />
				<xsl:for-each select="sc:item(text(),.)">

					<!-- Get Header Text Colour Item -->
					<xsl:variable name="colourItem" select="sc:item(sc:fld('Header Text Colour',.),.)" />

					<div class="contentBlock bg">
						<!-- default class. Applies a min-height-->
						<xsl:choose>
							<xsl:when test="sc:fld('Background Image',.)">
								<xsl:attribute name="style">
									<xsl:text>background-image: url(</xsl:text>
									<xsl:value-of select="sc:fld('Background Image', ., 'src')" />
									<xsl:text>);</xsl:text>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<!-- No background image so no min-height -->
								<xsl:attribute name="class">contentBlock</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>

						<div class="contentContainer">
							
							<!-- Spotlight text etc. h2 is hard-coded for this page (according to GA) -->
							<div class="spotlight">
									<h2>Cabin Details</h2>
									<xsl:if test="sc:fld('Header Text',.)">
										<xsl:variable name="colour" select="sc:fld('Hex Value',$colourItem)" />
										<h3>
											<xsl:if test="$colour">
												<xsl:attribute name="style">
													<xsl:value-of select="concat('color: #',$colour)" />
													<xsl:text>;</xsl:text>
												</xsl:attribute>
											</xsl:if>
											<sc:text field="Header Text" />
										</h3>
										<hr>
											<xsl:if test="$colour">
												<xsl:attribute name="style">
													<xsl:value-of select="concat(concat(concat('color: #',$colour),'; background-color: #'), $colour)" />
													<xsl:text>;</xsl:text>
												</xsl:attribute>
											</xsl:if>
										</hr>
									</xsl:if>
								</div>
							<div class="lhs w640">
								<!-- 'Overview' Text -->
								<xsl:if test="sc:fld('Overview',.)">
									<div class="cabinOverview">
										<sc:html field="Overview" />
									</div>
								</xsl:if>

								<!-- 'Features' Icons -->
								<xsl:if test="sc:fld('Features',.)">
									<div id="features">
										<div class="container">
											<xsl:for-each select="sc:Split('Features',.)">
												<xsl:for-each select="sc:item(text(),.)">
													<div class="feature">
														<xsl:attribute name="style">
															<xsl:text>background-image: url(</xsl:text>
															<xsl:value-of select="sc:fld('Image', ., 'src')" />
															<xsl:text>);background-repeat: no-repeat;</xsl:text>
														</xsl:attribute>
														<p><sc:text field="Description" /></p>
													</div>
												</xsl:for-each>
											</xsl:for-each>
										</div>
									</div>
								</xsl:if>
							
								<!-- 'Images Videos' thumbnails -->
								<xsl:if test="sc:fld('Images Videos',.)">
									<div class="video thumbnails">
										<xsl:for-each select="sc:Split('Images Videos',.)">
											<xsl:variable name="pos" select="position()" />
											<xsl:for-each select="sc:item(text(),.)">
													<xsl:choose>
														<xsl:when test="sc:ToLower(@template) ='image light box'">
															<xsl:choose>
																<xsl:when test="sc:fld('Disable lightbox',.) = '1'">
																	<div class="item">
																		<sc:image field="Image" w="214" h="159"></sc:image>
																		<xsl:if test="sc:field('Caption',.)">
																			<div class="caption">
																				<p><sc:text field="Caption"></sc:text></p>
																			</div>
																		</xsl:if>
																	</div>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:call-template name="Light-Box-Image">
																		<xsl:with-param name="index" select="$counter" />
																		<xsl:with-param name="pos" select="$pos" />
																	</xsl:call-template>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:when>
														<xsl:when test="sc:ToLower(@template) ='video light box'">
															<xsl:call-template name="Light-Box-Video-No-Caption">
																<xsl:with-param name="index" select="$counter" />
																<xsl:with-param name="pos" select="$pos" />
															</xsl:call-template>
														</xsl:when>
													</xsl:choose>
											</xsl:for-each>
										</xsl:for-each>
									</div>
								</xsl:if>
							
								<!-- 'Cabin Details' HTML -->
								<div id="cabinDetails">
									<sc:html field="Cabin Details" />
								</div>
							</div>
							
							<!-- RHS Info -->
							<div class="rhs w300">
								<xsl:if test="sc:fld('RHS Heading',.)">
									<h3>
										<sc:text field="RHS Heading" />
									</h3>
									<div>
										<sc:html field="RHS Text" />
									</div>
									<xsl:if test="sc:fld('RHS Heading',.)">
										<div class="images">
											<xsl:for-each select="sc:Split('Slideshow Images',.)">
												<xsl:choose>
													<xsl:when test="position() mod 2 = 1">
														<xsl:for-each select="sc:item(text(),.)">
															<div class="left">
																<xsl:variable name="alt">
																	<xsl:value-of select="sc:fld('alt',.)" />
																</xsl:variable>
																<img src="{concat(sc:GetMediaUrl(.),'?mw=140&amp;mh=105')}">
																	<xsl:attribute name="alt">
																		<xsl:value-of select="$alt"/>
																	</xsl:attribute>
																</img>
															</div>
														</xsl:for-each>
													</xsl:when>
													<xsl:otherwise>
														<xsl:for-each select="sc:item(text(),.)">
															<div>
																<xsl:variable name="alt">
																	<xsl:value-of select="sc:fld('alt',.)" />
																</xsl:variable>
																<img src="{concat(sc:GetMediaUrl(.),'?mw=140&amp;mh=105')}">
																	<xsl:attribute name="alt">
																		<xsl:value-of select="$alt"/>
																	</xsl:attribute>
																</img>
															</div>
														</xsl:for-each>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</div>
									</xsl:if>
								</xsl:if>
							</div>
						</div>
					</div>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:if>
	
	<!--<xsl:variable name="customVariable" select="fh:customMethod('someParameter')"></xsl:variable>-->
</xsl:template>

</xsl:stylesheet>