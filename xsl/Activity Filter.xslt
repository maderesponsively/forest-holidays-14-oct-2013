﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
	File: Activity Filter.xslt                                                   
	Created by: sitecore\paulh                                       
	Created: 06/09/2012 15:28:50                                               
	Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

	<!-- Includes -->
	<xsl:include href="includes/Functions.xslt"/>
	
	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8" />

	<!-- parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>

	<!-- entry point -->
	<xsl:template match="*">
		<xsl:apply-templates select="$sc_item" mode="main"/>
	</xsl:template>

	<!--==============================================================-->
	<!-- main                                                         -->
	<!--==============================================================-->
	<xsl:template match="*" mode="main">
		<div class ="layoutContainer">
			<div id="locationMap">
				<div id="activity">
					<p>Looking to do a specific activity on your holiday? Please use the filter to find your location</p>
					<div id="filter">
						<a href="#">
							<span>Filter locations by activity</span>
							<img src="/img/filter-arrow-up.png" alt="Up" />
						</a>
						<div id="activityList">
							<div class="activity">
								<label>
									<input type="checkbox" name="actGroup" value="all" />
									<span>All activities</span>
								</label>
							</div>
							<!-- /.activity -->
							<xsl:for-each select="sc:item(fh:ConfigValue('ActivitiesItemId'),.)/item">
								<div class="activity">
									<label>
										<input type="checkbox" name="actGroup">
											<xsl:attribute name="value">
												<xsl:value-of select="@name" />
											</xsl:attribute>
										</input>
										<span>
											<xsl:value-of select="@name" />
										</span>
										<div class="icon">
											<sc:image field="Activity Icon" alt=""></sc:image>
										</div>
									</label>
								</div>
								<!-- /.activity -->

							</xsl:for-each>
						</div>
						<!-- /#activityList -->
					</div>
					<!-- /#filter -->
				</div>
				<!-- /.activity -->
				<div id="map">
					<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
						<xsl:if test="sc:fld('Show on Map', .)">
							<xsl:text>&#10;&#09;&#09;</xsl:text>
							<div>
								<xsl:attribute name="class">
									<xsl:text>flag </xsl:text>
									<xsl:value-of select="fh:lowerLocationName(@name)"/>
									<xsl:text> inactive</xsl:text>
								</xsl:attribute>
								<sc:link field="Location Url">
									<span>
										<xsl:value-of select="sc:fld('Location Name',.)" />
									</span>
								</sc:link>
							</div>
						</xsl:if>
					</xsl:for-each>
				</div>
			</div>
			<!-- /.locationmap -->
		</div>
		<!-- /.layoutcontainer -->

		<script type="text/javascript">
			<![CDATA[$(document).ready(function () {
				$("#activity #filter a img").attr("src", "/img/filter-arrow-down.png");
				// Toggle checked attribute on all activities
				$('.activity label input[value=all]').click(function () {
					$(this).parents("#activityList").find(':checkbox').attr('checked', this.checked);
				});
				
				$("#activityList input").click(function (e) {
					// Get all elements, except "select all"
					var allButFirst = $("#activityList input:checkbox").not(":eq(0)");
					// Get select all checkbox
					var all = $(".activity label input[value=all]");

					// If every checkbox is checked
					if (all.is(":checked")) {
						if (!$(this).is(all)) {
							all.prop("checked", false);
						}
					}

					// Filters each value to see if all checked, apart from top
					if (allButFirst.filter(":not(:checked)").length === 0) {
						all.prop("checked", true);
					}

				]]>
				<xsl:for-each select="sc:item(fh:ConfigValue('ActivitiesItemId'),.)/item">
					<![CDATA[
						var is]]><xsl:value-of select="@name"/><![CDATA[Checked = $('input[value="]]><xsl:value-of select="@name"/><![CDATA["]:checked').length > 0;
						fadeFlag]]><xsl:value-of select="@name"/><![CDATA[(is]]><xsl:value-of select="@name"/><![CDATA[Checked);
					]]>
				</xsl:for-each>	
				<![CDATA[
				});

				$('#activity #filter a').toggle(function () {
					$("#activity #filter a img").attr("src", "/img/filter-arrow-up.png");
					$('#activity #filter #activityList').slideDown('fast');
					}, function () {
					$("#activity #filter a img").attr("src", "/img/filter-arrow-down.png");
					$('#activity #filter #activityList').slideUp('fast');
				});
				
				]]>
				<xsl:for-each select="sc:item(fh:ConfigValue('ActivitiesItemId'),.)/item">
					<![CDATA[
					$('.activity label input[value=]]><xsl:value-of select="@name"/><![CDATA[]').click(function () {
						$(this).attr('checked', this.checked);
						var is]]><xsl:value-of select="@name"/><![CDATA[Checked = $('input[value="]]><xsl:value-of select="@name"/><![CDATA["]:checked').length > 0;
						fadeFlag]]><xsl:value-of select="@name"/><![CDATA[(is]]><xsl:value-of select="@name"/><![CDATA[Checked);
					});
					]]>
				</xsl:for-each>
				<![CDATA[
			})
			
			]]>
			<xsl:for-each select="sc:item(fh:ConfigValue('ActivitiesItemId'),.)/item">
				<xsl:variable name="ActivityItemName" select="@name" />
				<![CDATA[
					function fadeFlag]]><xsl:value-of select="$ActivityItemName"/><![CDATA[(active) {
						if (active) {]]>
						<!-- All sites with this Activity -->
						<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
							<xsl:variable name="LocationName" select="fh:lowerLocationName(@name)" />
							<!--see if the activity exists in this location-->
							<xsl:for-each select="sc:Split('Activities',.)">
								<xsl:for-each select="sc:item(text(),.)">
									<xsl:if test="$ActivityItemName = @name">
										<!-- Add this site -->
										<![CDATA[$("#map .]]><xsl:value-of select="$LocationName"/><![CDATA[").removeClass("inactive");]]>
										<xsl:text>&#10;&#09;&#09;</xsl:text>
									</xsl:if>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:for-each>
						<![CDATA[} else {]]>
						<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
							<xsl:variable name="LocationName" select="fh:lowerLocationName(@name)" />
							<!--see if the activity exists in this location-->
							<xsl:for-each select="sc:Split('Activities',.)">
								<xsl:for-each select="sc:item(text(),.)">
									<xsl:if test="$ActivityItemName = @name">
										<!-- Add this site -->
										<![CDATA[$("#map .]]><xsl:value-of select="$LocationName"/><![CDATA[").addClass("inactive");]]>
										<xsl:text>&#10;&#09;&#09;</xsl:text>
									</xsl:if>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:for-each>
						<![CDATA[
						}
					}
					]]>
				</xsl:for-each>
		</script>
	</xsl:template>

</xsl:stylesheet>


