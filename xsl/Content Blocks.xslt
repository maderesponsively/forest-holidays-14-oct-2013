﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
	File: Content Blocks.xslt                                                   
	Created by: sitecore\paulh                                       
	Created: 16/08/2012 15:20:22                                               
	Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

	<!-- Includes -->

	<xsl:include href="includes/Locations.xslt"/>
	<xsl:include href="includes/Light Box Thumbnails.xslt"/>
	<xsl:include href="includes/Image Gallery.xslt"/>
	<xsl:include href="includes/Testimonials.xslt"/>
	<xsl:include href="includes/Offer.xslt"/>
	<xsl:include href="includes/Slideshow.xslt"/>
	<xsl:include href="includes/Events.xslt"/>
	<xsl:include href="includes/Buttons.xslt"/>
	<xsl:include href="includes/In the Media.xslt"/>
	<xsl:include href="includes/Additional Information.xslt"/>
	<xsl:include href="includes/Thing to do.xslt"/>
	
	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8" />

	<!-- parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>


	<!-- entry point -->
	<xsl:template match="*">
		<xsl:apply-templates select="$sc_item" mode="main"/>
	</xsl:template>

	<!--==============================================================-->
	<!-- main                                                         -->
	<!--==============================================================-->
	<xsl:template match="*" mode="main">
		<xsl:if test="sc:fld('Content Blocks',.)">
			<div id="contentBlockContainer">
				<xsl:for-each select="sc:Split('Content Blocks',.)">
					<xsl:variable name="counter" select="position()" />
					<xsl:for-each select="sc:item(text(),.)">

						<!-- Get Header Text Colour Item -->
						<xsl:variable name="colourItem" select="sc:item(sc:fld('Header Text Colour',.),.)" />

						<div class="contentBlock bg">
							<!-- default class. Applies a min-height-->
							<xsl:choose>
								<xsl:when test="sc:fld('Background Image',.)">
									<xsl:attribute name="style">
										<xsl:text>background-image: url(</xsl:text>
										<xsl:value-of select="sc:fld('Background Image', ., 'src')" />
										<xsl:text>);</xsl:text>
									</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<!-- No background image so no min-height -->
									<xsl:attribute name="class">contentBlock</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>

							<div class="contentContainer">
								<!-- Spotlight text etc. Only if 'Header Spotlight Text' is not empty -->
								<xsl:if test="sc:fld('Header Spotlight Text',.)">
									<div class="spotlight">
										<h2>
											<sc:text field="Header Spotlight Text" />
										</h2>
										<xsl:if test="sc:fld('Header Text',.)">
											<xsl:variable name="colour" select="sc:fld('Hex Value',$colourItem)" />
											<h3>
												<xsl:if test="$colour">
													<xsl:attribute name="style">
														<xsl:value-of select="concat('color: #',$colour)" />
														<xsl:text>;</xsl:text>
													</xsl:attribute>
												</xsl:if>
												<sc:text field="Header Text" />
											</h3>
											<hr>
												<xsl:if test="$colour">
													<xsl:attribute name="style">
														<xsl:value-of select="concat(concat(concat('color: #',$colour),'; background-color: #'), $colour)" />
														<xsl:text>;</xsl:text>
													</xsl:attribute>
												</xsl:if>
											</hr>
										</xsl:if>
									</div>
								</xsl:if>

								<xsl:choose>
									<xsl:when test="sc:ToLower(@template) ='content block - locations'">
										<xsl:call-template name="locations">
											<xsl:with-param name="itemIndex" select="$counter" />
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - lightbox thumbnails'">
										<xsl:call-template name="Light-Box-Thumbnails">
											<xsl:with-param name="itemIndex" select="$counter" />
											<!-- Pass the index of this content block for use as a unique id -->
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - image'">
										<xsl:call-template name="image">
											<xsl:with-param name="itemIndex" select="$counter" />
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - slideshow'">
										<xsl:call-template name="slideshow">
											<xsl:with-param name="itemIndex" select="$counter" />
											<!-- Pass the index of this content block for use as a unique id -->
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - video gallery'">
										<xsl:call-template name="videoGallery">
											<xsl:with-param name="itemIndex" select="$counter" />
											<!-- Pass the index of this content block for use as a unique id -->
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - image gallery'">
										<xsl:call-template name="imageGallery">
											<xsl:with-param name="itemIndex" select="$counter" />
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - offers'">
										<xsl:call-template name="offers">
											<xsl:with-param name="itemIndex" select="$counter" />
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="sc:ToLower(@template) ='content block - things to do'">
										<xsl:call-template name="thingsToDo">
											<xsl:with-param name="itemIndex" select="$counter" />
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="bodyText">
											<xsl:with-param name="itemIndex" select="$counter" />
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
					</xsl:for-each>
				</xsl:for-each>
			</div>
		</xsl:if>

	</xsl:template>



	<!--==============================================================-->
	<!-- Image                                                        -->
	<!--==============================================================-->
	<xsl:template name="image">
		<xsl:param name="itemIndex" />
		<div>
			<xsl:attribute name="class">
				<xsl:value-of select="sc:fld('Value', sc:item(sc:field('Text Float',.),.))" />
			</xsl:attribute>
			<div class="bodyText">
				<sc:text field="Body Text" />
				<xsl:if test="sc:fld('Video',.)">
					<div class="video thumbnails">
						<xsl:if test="sc:fld('Video',.)">
							<xsl:for-each select="sc:Split('Video',.)">
								<xsl:variable name="pos" select="position()" />
								<xsl:for-each select="sc:item(text(),.)">
									<div class="item">
										<div class="play">
											<a class="fancybox iframe">
												<xsl:attribute name="rel">
													<xsl:choose>
														<xsl:when test="sc:fld('Gallery',.) = '1'">
															<!--DO NOT be part of a gallery-->
															<!--Video has unique gallery ID-->
															<xsl:value-of select="concat(concat('prettyPhoto[galleryP1',$pos),']')"/>
														</xsl:when>
														<xsl:otherwise>
															<!--Normal Index, part of the gallery-->
															<xsl:value-of select="concat(concat('prettyPhoto[galleryP1',$itemIndex),']')"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:attribute name="href">
													<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
													<xsl:text>?iframe=true&#38;width=</xsl:text>
													<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
													<xsl:text>&#38;height=</xsl:text>
													<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
												</xsl:attribute>
												<img alt="" src="/img/video-play-icon.png"></img>
											</a>
										</div>
										<a class="fancybox iframe">
											<xsl:attribute name="rel">
												<xsl:choose>
													<xsl:when test="sc:fld('Gallery',.) = '1'">
														<!--DO NOT be part of a gallery-->
														<!--Video has unique gallery ID-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryP2',$pos),']')"/>
													</xsl:when>
													<xsl:otherwise>
														<!--Normal Index, part of the gallery-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryP2',$itemIndex),']')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
												<xsl:text>?iframe=true&#38;width=</xsl:text>
												<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
												<xsl:text>&#38;height=</xsl:text>
												<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
											</xsl:attribute>
											<img alt="">
												<xsl:attribute name="src">
													<xsl:value-of select="sc:fld('Thumbnail Image',.,'src')"></xsl:value-of>
													<xsl:text>?w=214&#38;h=159</xsl:text>
												</xsl:attribute>
											</img>
										</a>
									</div>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:if>
					</div>
				</xsl:if>
			</div>
			<xsl:call-template name="renderEvents" />
			<xsl:call-template name="renderTestimonials" />
			<xsl:call-template name="inTheMedia" />
			<xsl:call-template name="additionalInformation" />
			<xsl:call-template name="renderButtons"/>
		</div>
		<div class="Left">
			<sc:image field="Image" mw="460"></sc:image>
		</div>
	</xsl:template>

	<!--==============================================================-->
	<!-- Slideshow                                                    -->
	<!--==============================================================-->
	<xsl:template name="slideshow">
		<xsl:param name="itemIndex" />
		<div>
			<xsl:attribute name="class">
				<xsl:value-of select="sc:fld('Value', sc:item(sc:field('Text Float',.),.))" />
			</xsl:attribute>
			<div class="bodyText">
				<sc:text field="Body Text" />
				<xsl:if test="sc:fld('Video',.)">
					<div class="video thumbnails">
						<xsl:if test="sc:fld('Video',.)">
							<xsl:for-each select="sc:Split('Video',.)">
								<xsl:variable name="pos" select="position()" />
								<xsl:for-each select="sc:item(text(),.)">
									<div class="item">
										<div class="play">
											<a class="fancybox iframe">
												<xsl:attribute name="rel">
													<xsl:choose>
														<xsl:when test="sc:fld('Gallery',.) = '1'">
															<!--DO NOT be part of a gallery-->
															<!--Video has unique gallery ID-->
															<xsl:value-of select="concat(concat('prettyPhoto[galleryS1',$pos),']')"/>
														</xsl:when>
														<xsl:otherwise>
															<!--Normal Index, part of the gallery-->
															<xsl:value-of select="concat(concat('prettyPhoto[galleryS1',$itemIndex),']')"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:attribute name="href">
													<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
													<xsl:text>?iframe=true&#38;width=</xsl:text>
													<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
													<xsl:text>&#38;height=</xsl:text>
													<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
												</xsl:attribute>
												<img alt="" src="/img/video-play-icon.png"></img>
											</a>
										</div>
										<a class="fancybox iframe">
											<xsl:attribute name="rel">
												<xsl:choose>
													<xsl:when test="sc:fld('Gallery',.) = '1'">
														<!--DO NOT be part of a gallery-->
														<!--Video has unique gallery ID-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryS2',$pos),']')"/>
													</xsl:when>
													<xsl:otherwise>
														<!--Normal Index, part of the gallery-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryS2',$itemIndex),']')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
												<xsl:text>?iframe=true&#38;width=</xsl:text>
												<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
												<xsl:text>&#38;height=</xsl:text>
												<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
											</xsl:attribute>
											<img alt="">
												<xsl:attribute name="src">
													<xsl:value-of select="sc:fld('Thumbnail Image',.,'src')"></xsl:value-of>
													<xsl:text>?w=214&#38;h=159</xsl:text>
												</xsl:attribute>
											</img>
										</a>
									</div>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:if>
					</div>
				</xsl:if>
			</div>
			<xsl:call-template name="renderEvents" />
			<xsl:call-template name="renderTestimonials" />
			<xsl:call-template name="inTheMedia" />
			<xsl:call-template name="additionalInformation" />
			<xsl:call-template name="renderButtons"/>
		</div>
		<div class="Left">
			<xsl:call-template name="renderSlideshow">
				<xsl:with-param name="index" select="$itemIndex" />
			</xsl:call-template>
		</div>
	</xsl:template>

	<!--==============================================================-->
	<!-- Video Gallery                                                -->
	<!--==============================================================-->
	<xsl:template name="videoGallery">
		<xsl:param name="itemIndex" />
		<div class="video thumbnails">
			<xsl:for-each select="sc:Split('Videos',.)">
				<xsl:variable name="pos" select="position()" />
				<xsl:for-each select="sc:item(text(),.)">
					<xsl:call-template name="Light-Box-Video">
						<xsl:with-param name="index" select="$itemIndex" />
						<xsl:with-param name="pos" select="$pos" />
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>

	<!--==============================================================-->
	<!-- Image Gallery                                                -->
	<!--==============================================================-->
	<xsl:template name="imageGallery">
		<xsl:param name="itemIndex" />
		<xsl:if test="sc:fld('Body Text',.)">
			<div class="bodyText full">
				<sc:text field="Body Text" show-title-when-blank="true" />
				<xsl:if test="sc:fld('Video',.)">
					<div class="video thumbnails">
						<xsl:if test="sc:fld('Video',.)">
							<xsl:for-each select="sc:Split('Video',.)">
								<xsl:variable name="pos" select="position()" />
								<xsl:for-each select="sc:item(text(),.)">
									<div class="item">
										<div class="play">
											<a class="fancybox iframe">
												<xsl:attribute name="rel">
													<xsl:choose>
														<xsl:when test="sc:fld('Gallery',.) = '1'">
															<!--DO NOT be part of a gallery-->
															<!--Video has unique gallery ID-->
															<xsl:value-of select="concat(concat('prettyPhoto[galleryI1',$pos),']')"/>
														</xsl:when>
														<xsl:otherwise>
															<!--Normal Index, part of the gallery-->
															<xsl:value-of select="concat(concat('prettyPhoto[galleryI1',$itemIndex),']')"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:attribute>
												<xsl:attribute name="href">
													<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
													<xsl:text>?iframe=true&#38;width=</xsl:text>
													<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
													<xsl:text>&#38;height=</xsl:text>
													<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
												</xsl:attribute>
												<img alt="" src="/img/video-play-icon.png"></img>
											</a>
										</div>
										<a class="fancybox iframe">
											<xsl:attribute name="rel">
												<xsl:choose>
													<xsl:when test="sc:fld('Gallery',.) = '1'">
														<!--DO NOT be part of a gallery-->
														<!--Video has unique gallery ID-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryI2',$pos),']')"/>
													</xsl:when>
													<xsl:otherwise>
														<!--Normal Index, part of the gallery-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryI2',$itemIndex),']')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
												<xsl:text>?iframe=true&#38;width=</xsl:text>
												<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
												<xsl:text>&#38;height=</xsl:text>
												<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
											</xsl:attribute>
											<img alt="">
												<xsl:attribute name="src">
													<xsl:value-of select="sc:fld('Thumbnail Image',.,'src')"></xsl:value-of>
													<xsl:text>?w=214&#38;h=159</xsl:text>
												</xsl:attribute>
											</img>
										</a>
									</div>
								</xsl:for-each>
							</xsl:for-each>
						</xsl:if>
					</div>
				</xsl:if>
			</div>
		</xsl:if>
		<div class="imageGallery thumbnails">
			<xsl:for-each select="sc:Split('Image Light Boxes',.)">
				<xsl:variable name="pos" select="position()" />
				<xsl:for-each select="sc:item(text(),.)">
					<xsl:call-template name="Image-Gallery">
						<xsl:with-param name="index" select="$itemIndex" />
						<xsl:with-param name="pos" select="$pos" />
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>
	
	<!--==============================================================-->
	<!-- Just Text                                                    -->
	<!--==============================================================-->
	<xsl:template name="bodyText">
		<xsl:param name="itemIndex" />
		<div class="bodyText full">
			<sc:text field="Body Text" show-title-when-blank="true" />
			<xsl:if test="sc:fld('Video',.)">
				<div class="video thumbnails">
					<xsl:if test="sc:fld('Video',.)">
						<xsl:for-each select="sc:Split('Video',.)">
							<xsl:variable name="pos" select="position()" />
							<xsl:for-each select="sc:item(text(),.)">
								<div class="item">
									<div class="play">
										<a class="fancybox iframe">
											<xsl:attribute name="rel">
												<xsl:choose>
													<xsl:when test="sc:fld('Gallery',.) = '1'">
														<!--DO NOT be part of a gallery-->
														<!--Video has unique gallery ID-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryT1',$pos),']')"/>
													</xsl:when>
													<xsl:otherwise>
														<!--Normal Index, part of the gallery-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryT1',$itemIndex),']')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
												<xsl:text>?iframe=true&#38;width=</xsl:text>
												<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
												<xsl:text>&#38;height=</xsl:text>
												<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
											</xsl:attribute>
											<img alt="" src="/img/video-play-icon.png"></img>
										</a>
									</div>
									<a class="fancybox iframe">
										<xsl:attribute name="rel">
											<xsl:choose>
												<xsl:when test="sc:fld('Gallery',.) = '1'">
													<!--DO NOT be part of a gallery-->
													<!--Video has unique gallery ID-->
													<xsl:value-of select="concat(concat('prettyPhoto[galleryT2',$pos),']')"/>
												</xsl:when>
												<xsl:otherwise>
													<!--Normal Index, part of the gallery-->
													<xsl:value-of select="concat(concat('prettyPhoto[galleryT2',$itemIndex),']')"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<xsl:attribute name="href">
											<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
											<xsl:text>?iframe=true&#38;width=</xsl:text>
											<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
											<xsl:text>&#38;height=</xsl:text>
											<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
										</xsl:attribute>
										<img alt="">
											<xsl:attribute name="src">
												<xsl:value-of select="sc:fld('Thumbnail Image',.,'src')"></xsl:value-of>
												<xsl:text>?w=214&#38;h=159</xsl:text>
											</xsl:attribute>
										</img>
									</a>
								</div>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:if>
				</div>
			</xsl:if>
		</div>
	</xsl:template>

	<!--==============================================================-->
	<!-- Offers                                                       -->
	<!--==============================================================-->
	<xsl:template name="offers">
		<xsl:param name="itemIndex" />
		<div class="offers">
			<xsl:for-each select="sc:Split('Offers',.)">
				<xsl:for-each select="sc:item(text(),.)">
					<xsl:call-template name="renderOffer" />
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>

	<!--==============================================================-->
	<!-- Locations                                                    -->
	<!--==============================================================-->
	<xsl:template name="locations">
		<xsl:param name="itemIndex" />
		<div>
			<xsl:attribute name="class">
				<xsl:value-of select="sc:field('Value', sc:item(sc:field('Text Float',.),.))" />
			</xsl:attribute>
			<div class="bodyText">
				<sc:text field="Body Text" />
				<div class="video thumbnails">
					<xsl:if test="sc:fld('Video',.)">
						<xsl:for-each select="sc:Split('Video',.)">
							<xsl:variable name="pos" select="position()" />
							<xsl:for-each select="sc:item(text(),.)">
								<div class="item">
									<div class="play">
										<a class="fancybox iframe">
											<xsl:attribute name="rel">
												<xsl:choose>
													<xsl:when test="sc:fld('Gallery',.) = '1'">
														<!--DO NOT be part of a gallery-->
														<!--Video has unique gallery ID-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryL1',$pos),']')"/>
													</xsl:when>
													<xsl:otherwise>
														<!--Normal Index, part of the gallery-->
														<xsl:value-of select="concat(concat('prettyPhoto[galleryL1',$itemIndex),']')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
												<xsl:text>?iframe=true&#38;width=</xsl:text>
												<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
												<xsl:text>&#38;height=</xsl:text>
												<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
											</xsl:attribute>
											<img alt="" src="/img/video-play-icon.png"></img>
										</a>
									</div>
									<a class="fancybox iframe">
										<xsl:attribute name="rel">
											<xsl:choose>
												<xsl:when test="sc:fld('Gallery',.) = '1'">
													<!--DO NOT be part of a gallery-->
													<!--Video has unique gallery ID-->
													<xsl:value-of select="concat(concat('prettyPhoto[galleryL2',$pos),']')"/>
												</xsl:when>
												<xsl:otherwise>
													<!--Normal Index, part of the gallery-->
													<xsl:value-of select="concat(concat('prettyPhoto[galleryL2',$itemIndex),']')"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>										
										<xsl:attribute name="href">
											<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
											<xsl:text>?iframe=true&#38;width=</xsl:text>
											<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
											<xsl:text>&#38;height=</xsl:text>
											<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
										</xsl:attribute>
										<img alt="">
											<xsl:attribute name="src">
												<xsl:value-of select="sc:fld('Thumbnail Image',.,'src')"></xsl:value-of>
												<xsl:text>?w=214&#38;h=159</xsl:text>
											</xsl:attribute>
										</img>
									</a>
								</div>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:if>
				</div>
			</div>
			<xsl:call-template name="renderEvents" />
			<xsl:call-template name="renderTestimonials" />
			<xsl:call-template name="inTheMedia" />
			<xsl:call-template name="additionalInformation" />
			<xsl:call-template name="renderButtons"/>
		</div>
		<xsl:call-template name="renderLocationMap" />
	</xsl:template>

	<!--==============================================================-->
	<!-- Things to Do                                                 -->
	<!--==============================================================-->
	<xsl:template name="thingsToDo">
		<div class="thingsToDo">
			<xsl:for-each select="sc:Split('Things to Do',.)">
				<xsl:variable name="pos" select="position()" />
				<xsl:for-each select="sc:item(text(),.)">
					<xsl:call-template name="Things-to-Do">
						<xsl:with-param name="pos" select="$pos" />
					</xsl:call-template>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>
