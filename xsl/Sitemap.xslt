﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Sitemap.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 14/11/2012 13:18:06                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<div id="sitemapContainer">
		<ul class="level1">
			<xsl:call-template name="sitemap-outline"/>
		</ul>
	</div>
</xsl:template>

	<xsl:template name="sitemap-outline">
		<xsl:for-each select="$home/item">
			<xsl:call-template name="ShowChild">
				<xsl:with-param name="level" select="1" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>


	<!--***************************************-->
	<!--              ShowChild                -->
	<!--***************************************-->
	<xsl:template name="ShowChild">
		<xsl:param name="level" />
		<xsl:if test="sc:fld('Sitemap',.)='1'">
			<li>
				<xsl:choose>
					<xsl:when test="@template = 'link page'" >
						<xsl:if test="sc:field('Navigation Link Text',.)">
							<a href="{sc:path(.)}">
								<sc:text field="Navigation Link Text" />
							</a>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<sc:link select=".">
							<sc:text field="Navigation Link Text"/>
						</sc:link>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="count(./item[sc:fld('Sitemap',.)='1']) > 0">
					<xsl:variable name="lvl" select="$level + 1" />
					<ul>
						<xsl:attribute name="class">
							<xsl:value-of select="concat('level',$lvl)"/>
						</xsl:attribute>
						<xsl:for-each select="./item">
							<xsl:call-template name="ShowChild">
								<xsl:with-param name="level" select="$lvl" />
							</xsl:call-template>
						</xsl:for-each>
					</ul>
				</xsl:if>
			</li>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>