﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Recommended Extra Categories.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 05/11/2012 16:18:46                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<div class="extraCategory">
		<div class="extraCategoryName">
			<h3>
				<xsl:value-of select="sc:field('Recommended Title',$siteSettings)"/>
			</h3>
		</div>
	</div>
	<ul>
		<xsl:for-each select="sc:Split('Recommended Extra Categories',$siteSettings)">
			<xsl:variable name="pos" select="position()" />
			<xsl:for-each select="sc:item(text(),.)">
				<xsl:if test="$pos &lt; 5">
					<li>
						<div class="item">
							<xsl:attribute name="style">
								<xsl:text>background-image: url(</xsl:text>
								<xsl:value-of select="sc:fld('Category Image', ., 'src')" />
								<xsl:text>?w=220&amp;h=166);</xsl:text>
							</xsl:attribute>
							<xsl:if test="$pos = 4">
								<xsl:attribute name="class">item end</xsl:attribute>
							</xsl:if>
							<a class="corner">
								<xsl:attribute name="href">
									#<xsl:value-of select="sc:ToLower(sc:fld('Roots Category Code',.))"></xsl:value-of>
								</xsl:attribute>
								<img alt="{sc:fld('Category Image',.,'alt')}" src="/img/extra-corner-icon.png"></img>
							</a>
							<xsl:if test="sc:field('Recommended Text',.)">
							<a class="caption">
								<xsl:attribute name="href">
									#<xsl:value-of select="sc:ToLower(sc:fld('Roots Category Code',.))"></xsl:value-of>
								</xsl:attribute>
								<div class="caption">
									<p>
										<xsl:value-of select="sc:fld('Recommended Text',.)" />
									</p>
								</div>
							</a>
							</xsl:if>
						</div>
					</li>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</ul>
</xsl:template>

</xsl:stylesheet>