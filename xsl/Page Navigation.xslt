﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Page navigation.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 20/08/2012 11:16:03                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8" />

	<!-- parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>

	<!-- variables -->
	<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />
	<xsl:variable name="SiteHomeID" select="fh:GetSiteRootItem($sc_currentitem/@id)" />
	<xsl:variable name="SiteHomeItem" select="sc:item($SiteHomeID,.)" />
	<xsl:variable name="MyFeedbackItemId" select="'{2CB27008-FF54-4676-9D6F-5802161F59D2}'" />
	<xsl:variable name="MyFeedbackSettings" select="sc:item('{BFBEDB95-0968-43A1-AD9D-98C9C95D030A}',.)" />
	
	<!-- entry point -->
	<xsl:template match="*">
		<xsl:apply-templates select="$sc_item" mode="main"/>
	</xsl:template>

	<!--==============================================================-->
	<!-- main                                                         -->
	<!--==============================================================-->
	<xsl:template match="*" mode="main">
		
		<xsl:variable name="addExtrasPageID" select="sc:fld('Add Extras URL', $siteSettings, 'id')" />
		<xsl:variable name="chooseCabinPageID" select="sc:fld('Specific Cabin Booking URL', $siteSettings, 'id')" />
		<xsl:variable name="searchResultsPageID" select="sc:fld('Availability Results URL', $siteSettings, 'id')" />
		<xsl:choose>
			<!-- If this is the Add Extras Page OR Choose Cabin Page (Defined in SiteSettings)-->
			<xsl:when test="@id = $addExtrasPageID or @id = $chooseCabinPageID">
				<div id="bars">
					<div id="greenBar" class="basketGreenBar">
						<div class="layoutContainer">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="fh:MyBasketURL()" />
								</xsl:attribute>
								<span>Continue with booking</span>
							</a>
						</div>
					</div>
					<!-- /#greenBar -->
					<!-- Only if this is the Add Extras Page (Defined in SiteSettings)-->
					<xsl:if test="@id = $addExtrasPageID">
						<nav id="sub-nav-bar" class="sub-nav-2 sticky">
							<ul class="layoutContainer">
								<xsl:value-of select="fh:ExtrasNavigation()" disable-output-escaping="yes"/>							
							</ul>
						</nav>
						<!-- /#stickyNav -->
					</xsl:if>
				</div>
				<!-- /#bars -->
				<xsl:if test="@id = $addExtrasPageID">
					<script type="text/javascript" src="/js/stickynav.js"></script>
					<script type="text/javascript">
						$(document).ready( function() {
						$("#bars").fixElement({appearAfterElement: 'header'});
						});
					</script>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="sc:field('Show Page Navigation',.) = '1'">
					
					<div id="bars">
						<!-- Normal Navigation -->
						<div id="greenBar">
							<div class="layoutContainer">
								<h1><sc:text field="Navigation Link Text"/></h1>
							</div>
						</div>
						<!-- Sub-Navigation -->
						<xsl:if test="sc:field('Show Sub Navigation',.) = '1'">
							<nav id="sub-nav-bar" class="sub-nav-1">
									<xsl:variable name="ids" select="sc:fld('My Account Navigation Items', $siteSettings)" />
									<xsl:choose>
										<!-- When this is the item or child of the item -->
										<xsl:when test="contains($ids, $sc_currentitem/@id) or contains($ids, $sc_currentitem/parent::item/@id)">
											<ul class="layoutContainer myAccount">
												<xsl:for-each select="sc:Split('My Account Navigation Items', $siteSettings)">
													<xsl:variable name="len" select="last()" />
													<xsl:variable name="pos" select="position()" />
													<xsl:for-each select="sc:item(text(),.)">
														<!-- Don't show My Feedback (unless this is My Feedback or its thank you page), Show everything else -->
														<xsl:if test="@id != $MyFeedbackItemId or ($sc_currentitem/@id = $MyFeedbackItemId or $sc_currentitem/parent::item/@id = $MyFeedbackItemId)">
															<li>
																<xsl:if test="$pos = $len">
																	<xsl:attribute name="class">last</xsl:attribute>
																</xsl:if>
																<a href="{sc:path(.)}">
																	<xsl:if test="@id = $sc_currentitem/@id or @id = $sc_currentitem/parent::item/@id">
																		<xsl:attribute name="class">active</xsl:attribute>
																	</xsl:if>
																	<xsl:value-of select="sc:fld('Navigation Link Text',.)"/>
																</a>
															</li>
														</xsl:if>
													</xsl:for-each>
												</xsl:for-each>
											</ul>
										</xsl:when>
										<xsl:otherwise>
											<ul class="layoutContainer">
												<li style="width: 96px;">
													<a href="{sc:path($SiteHomeItem)}">
														<xsl:if test="@id = $SiteHomeItem/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>Overview
													</a>
												</li>
												<li style="width: 126px;">
													<a href="{sc:path($SiteHomeItem/item[@key='accommodation'])}">
														<xsl:if test="@id = $SiteHomeItem/item[@key='accommodation']/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>Accommodation
													</a>
												</li>
												<li style="width: 125px;">
													<a href="{sc:path($SiteHomeItem/item[@key='whats-on-site'])}">
														<xsl:if test="@id = $SiteHomeItem/item[@key='whats-on-site']/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>What's on site?
													</a>
												</li>
												<li style="width: 125px;">
													<a href="{sc:path($SiteHomeItem/item[@key='whats-nearby'])}">
														<xsl:if test="@id = $SiteHomeItem/item[@key='whats-nearby']/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>What's nearby?
													</a>
												</li>
												<li style="width: 150px;">
													<a href="{sc:path($SiteHomeItem/item[@key='things-to-do'])}">
														<xsl:if test="$sc_currentitem/ancestor-or-self::item[@key='things-to-do']">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>Treats and activities
													</a>
												</li>
												<li style="width: 116px;">
													<a href="{sc:path($SiteHomeItem/item[@key='gallery'])}">
														<xsl:if test="@id = $SiteHomeItem/item[@key='gallery']/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>Photo gallery
													</a>
												</li>
												<li style="width: 120px;">
													<a href="{sc:path($SiteHomeItem/item[@key='maps-locations'])}">
														<xsl:if test="@id = $SiteHomeItem/item[@key='maps-locations']/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>Map &amp; location
													</a>
												</li>
												<li class="last" style="width: 90px;">
													<a href="{sc:path($SiteHomeItem/item[@key='reviews'])}">
														<xsl:if test="@id = $SiteHomeItem/item[@key='reviews']/@id">
															<xsl:attribute name="class">active</xsl:attribute>
														</xsl:if>Reviews <!--(<span id="reviews-number">0</span>) -->
													</a>
												</li>
											</ul>
										</xsl:otherwise>
									</xsl:choose>
							</nav>
						</xsl:if>
					</div>
				</xsl:if>
			<xsl:if test="sc:fld('Show Carousel',.) != '1'">
				<div>
					<xsl:choose>
						<xsl:when test="@id = $searchResultsPageID and fh:SessionValue('ShowSearchDiscountForm') = 'Y'">
							<xsl:attribute name="id">noShadow</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="id">navShadow</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>