﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<xsl:template name="additionalInformation">
		<xsl:if test="sc:fld('Additional Information',.)">
			<div class="additionalInformation">
				<xsl:for-each select="sc:Split('Additional Information',.)">
					<xsl:for-each select="sc:item(text(),.)">
						<div class="image">
							<sc:image field="Additional Information Image" w="45"></sc:image>
						</div>
						<div class="text">
							<sc:html field="Additional Information Text"/>
						</div>
					</xsl:for-each>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
