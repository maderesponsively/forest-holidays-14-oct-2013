﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<!-- Includes -->
	<xsl:include href="Light Box Image.xslt"/>
	<xsl:include href="Light Box Video.xslt"/>
	<xsl:include href="Light Box Empty.xslt"/>

	<!--==============================================================-->
	<!-- Lightbox Thumbnails                                          -->
	<!--==============================================================-->
	<xsl:template name="Light-Box-Thumbnails">
		<xsl:param name="itemIndex" />
		<div>
			<xsl:attribute name="class">
				<xsl:value-of select="sc:field('Value', sc:item(sc:field('Text Float',.),.))" />
			</xsl:attribute>
			<div class="bodyText">
				<sc:text field="Body Text" />
			</div>
			<xsl:call-template name="renderEvents" />
			<xsl:call-template name="renderButtons"/>
		</div>
		<div>
			<xsl:attribute name="class">thumbnails <xsl:value-of select="sc:field('Value', sc:item(sc:field('Text Float',.),.))" />
			</xsl:attribute>			
			<xsl:for-each select="sc:Split('Light Box Items',.)">
				<xsl:variable name="pos" select="position()" />
				<xsl:for-each select="sc:item(text(),.)">
					<xsl:choose>
						<xsl:when test="sc:ToLower(@template) ='image light box'">
							<xsl:call-template name="Light-Box-Image">
								<xsl:with-param name="index" select="$itemIndex" />
								<xsl:with-param name="pos" select="$pos" />
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="sc:ToLower(@template) ='video light box'">
							<xsl:call-template name="Light-Box-Video">
								<xsl:with-param name="index" select="$itemIndex" />
								<xsl:with-param name="pos" select="$pos" />
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="Light-Box-Empty">
								<xsl:with-param name="index" select="$itemIndex" />
								<xsl:with-param name="pos" select="$pos" />
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>

</xsl:stylesheet>