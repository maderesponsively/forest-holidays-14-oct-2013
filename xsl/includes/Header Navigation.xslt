﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

	<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />
	
	<xsl:template name="renderHeaderNavigation">
		<xsl:if test="sc:field('Header Navigation Items',$siteSettings)">
        	<a href="#" id="menuTrigger" class="closed"></a>
			<nav role="navigation">
				<ul>
					<xsl:for-each select="sc:Split('Header Navigation Items',$siteSettings)">
						<li>
							<xsl:attribute name="class">
								<xsl:choose>
									<xsl:when test="position() = '1'">cabins</xsl:when>
									<xsl:when test="position() = '2'">thingsToDo</xsl:when>
									<xsl:when test="position() = '3'">offers</xsl:when>
									<xsl:when test="position() = '4'">last</xsl:when>
								</xsl:choose>
							</xsl:attribute>
							<sc:link select="sc:item(text(),.)" >
								<sc:text field="Navigation Link Text" select="sc:item(text(),.)" />
							</sc:link>
						</li>
					</xsl:for-each>
				</ul>
			</nav>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>