﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

	<xsl:template name="renderEvents">
		<xsl:if test="sc:fld('Events',.)">
			<div class="events">
				<xsl:if test="sc:fld('Events Heading',.)">
					<div class="eventsHeading">
						<sc:text field="Events Heading" />
					</div>
				</xsl:if>
				<xsl:for-each select="sc:Split('Events',.)">
					<xsl:for-each select="sc:item(text(),.)">

						<xsl:if test="sc:fld('Title',.)">
							<div class="event">
								<xsl:choose>
									<xsl:when test="sc:fld('Start Date',.)">
										<div class="eventDates">
											<div class="startDateCalendar">
												<div class="startMonth"><xsl:value-of select="sc:formatdate(sc:fld('Start Date', .), 'MMM')" /></div>
												<div class="startDate"><xsl:value-of select="substring-after(sc:formatdate(sc:fld('Start Date', .), '/d'), '/')" /></div>
											</div>
											<xsl:if test="sc:fld('End Date',.)">
												<div class="dash">-</div>
												<div class="endDateCalendar">
													<div class="endMonth"><xsl:value-of select="sc:formatdate(sc:fld('End Date', .), 'MMM')" /></div>
													<div class="endDate"><xsl:value-of select="substring-after(sc:formatdate(sc:fld('End Date', .), '/d'), '/')" /></div>
												</div>
											</xsl:if>
										</div>
										<div class="eventTitle">
											<sc:link field="Hyperlink">
												<sc:text field="Title" />
											</sc:link>
										</div>
										<div class="eventDescription">
											<sc:text field="Description" />
										</div>
									</xsl:when>
									<xsl:otherwise>
										<div class="eventTitle">
											<sc:link field="Hyperlink">
												<sc:text field="Title" />
											</sc:link>
										</div>
										<div class="eventDescription wide">
											<sc:text field="Description" />
										</div>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</xsl:if>

					</xsl:for-each>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
