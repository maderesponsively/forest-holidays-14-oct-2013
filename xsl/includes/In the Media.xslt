﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<xsl:template name="inTheMedia">
		<xsl:if test="sc:fld('In the Media',.)">
			<div class="inTheMedia">
				<xsl:for-each select="sc:Split('In the Media',.)">
					<xsl:for-each select="sc:item(text(),.)">
						<div class="image">
							<sc:image field="Media Image" w="140"></sc:image>
						</div>
						<div class="title">
							<xsl:value-of select="sc:fld('Media Title',.)"/>
						</div>
						<div class="quote">
							<xsl:value-of select="sc:fld('Media Quote',.)"/>
						</div>
						<div class="author">
							<xsl:value-of select="sc:fld('Media Quote Author',.)"/>
						</div>
						<div class="link">
							<sc:link field="Media Article Link">Read full article</sc:link>
						</div>
					</xsl:for-each>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
