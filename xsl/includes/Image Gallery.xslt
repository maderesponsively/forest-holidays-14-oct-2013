﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<!--==============================================================-->
	<!-- Image Gallery                                          -->
	<!--==============================================================-->
	<xsl:template name="Image-Gallery">
		<xsl:param name="index" />
		<xsl:param name="pos" />
		<xsl:if test="sc:fld('Image',.,'src')">
			<div class="item">
				<div class="corner">
					<a class="fancybox img">
						<xsl:attribute name="href">
							<xsl:value-of select="concat('/',sc:fld('Image',.,'src'))"></xsl:value-of>
							<xsl:if test="sc:fld('Lightbox Fullsize',.) != '1'">
								<xsl:text>?w=600</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<xsl:if test="sc:fld('Descriptive Text',.)">
							<xsl:attribute name="title">
								<xsl:value-of select="sc:fld('Descriptive Text',.)"/>
							</xsl:attribute>
						</xsl:if>
						<img alt="{sc:fld('Image',.,'alt')}" src="/img/gallery-corner-icon.png" />
					</a>
				</div>
				<!-- ./corner -->
				<a class="fancybox img">
					<xsl:attribute name="rel">
						<xsl:choose>
							<xsl:when test="sc:fld('Gallery',.) = '1'">
								<!--DO NOT be part of a gallery-->
								<!--Image has unique gallery ID-->
								<xsl:value-of select="concat(concat('prettyPhoto[gallery',$pos),']')"/>
							</xsl:when>
							<xsl:otherwise>
								<!--Normal Index, part of the gallery-->
								<xsl:value-of select="concat(concat('prettyPhoto[gallery',$index),']')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:value-of select="concat('/',sc:fld('Image', ., 'src'))"></xsl:value-of>
						<xsl:if test="sc:fld('Lightbox Fullsize',.) != '1'">
							<xsl:text>?w=600</xsl:text>
						</xsl:if>
					</xsl:attribute>
					<xsl:if test="sc:fld('Descriptive Text',.)">
						<xsl:attribute name="title">
							<xsl:value-of select="sc:fld('Descriptive Text',.)"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="sc:fld('Thumbnail',.)">
							<sc:image field="Thumbnail" w="214" h="159"></sc:image>
						</xsl:when>
						<xsl:otherwise>
							<sc:image field="Image" w="214" h="159"></sc:image>
						</xsl:otherwise>
					</xsl:choose>
				</a>
				<xsl:if test="sc:field('Caption',.)">
					<div class="caption">
						<p>
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="concat('/',sc:fld('Image', ., 'src'))"></xsl:value-of>
									<xsl:if test="sc:fld('Lightbox Fullsize',.) != '1'">
										<xsl:text>?w=600</xsl:text>
									</xsl:if>
								</xsl:attribute>
								<sc:text field="Caption"></sc:text>
							</a>
						</p>
					</div>
				</xsl:if>
				<!-- /.caption -->
			</div>
			<!-- ./item -->
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
