﻿<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="sc fh">

	<!-- Includes -->
	<xsl:include href="../includes/Functions.xslt"/>
	
	<!--==============================================================-->
	<!-- Location Map                                                 -->
	<!--==============================================================-->
	<xsl:template name="renderLocationMap">
		<!--<script type="text/javascript" src="/js/homepagemap.js"></script>-->
		<div id="homeMap">
			<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
				<xsl:if test="sc:fld('Show on Map', .)">
					<xsl:text>&#10;&#09;&#09;</xsl:text>
					<div>
						<xsl:attribute name="class">
							<xsl:text>flag </xsl:text>
							<xsl:call-template name="ToLower">
								<xsl:with-param name="inputString" select="translate(sc:fld('Location Name',.),' ','')" />
							</xsl:call-template>
						</xsl:attribute>
						<sc:link field="Location Url">
							<span>
								<xsl:value-of select="sc:fld('Location Name',.)" />
							</span>
						</sc:link>
					</div>
				</xsl:if>
			</xsl:for-each>


			<xsl:text>&#10;&#09;</xsl:text>
			<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
				<xsl:if test="sc:fld('Show on Map', .)">
					<xsl:text>&#10;&#09;&#09;</xsl:text>
					<div>
						<xsl:attribute name="class">
							<xsl:call-template name="ToLower">
								<xsl:with-param name="inputString" select="translate(sc:fld('Location Name',.),' ','')" />
							</xsl:call-template>
							<xsl:text>Box</xsl:text>
						</xsl:attribute>
						<xsl:text>&#10;&#09;&#09;&#09;</xsl:text>
						<div class="boxContent">
							<xsl:text>&#10;&#09;&#09;&#09;&#09;</xsl:text>
							<sc:link field="Location Url">
								<sc:image field="Lightbox Image"></sc:image>
							</sc:link>
							<div class="content">
								<p><sc:text field="Lightbox Content" /></p>
							</div>
							<xsl:if test="sc:fld('Activities',.)">
								<ul class="icons">
									<xsl:for-each select="sc:Split('Activities',.)">
										<xsl:if test="position() &lt;= 8">
											<xsl:for-each select="sc:item(text(),.)">
												<xsl:variable name="activityName" select="sc:fld('Activity Name',.)"></xsl:variable>
												<li>
													<sc:image field="Activity Icon" Title="{$activityName}"></sc:image>
												</li>
											</xsl:for-each>
										</xsl:if>
									</xsl:for-each>
								</ul>
							</xsl:if>
						</div>
						<xsl:comment>/.boxContent</xsl:comment>
						<xsl:text>&#10;&#09;&#09;</xsl:text>
					</div>
					<xsl:comment>
						<xsl:text>/.</xsl:text>
						<xsl:call-template name="ToLower">
							<xsl:with-param name="inputString" select="translate(sc:fld('Location Name',.),' ','')" />
						</xsl:call-template>
						<xsl:text>Box</xsl:text>
					</xsl:comment>
					<xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>					
		</div>
	</xsl:template>

</xsl:stylesheet>
