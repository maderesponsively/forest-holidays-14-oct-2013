﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">
	<xsl:template name="ToLower">
		<xsl:param name="inputString"/>
		<xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyz'"/>
		<xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
	</xsl:template>

	<xsl:template name="RemoveSpaces">
		<xsl:param name="inputString"/>
		<xsl:value-of select="translate($inputString,' ','')"/>
	</xsl:template>
	
	<xsl:template name="br">
		<xsl:param name="text"/>
		<xsl:choose>
			<xsl:when test="contains($text,'&#xa;')">
				<xsl:value-of select="substring-before($text,'&#xa;')"/>
				<br/>
				<xsl:call-template name="br">
					<xsl:with-param name="text">
						<xsl:value-of select="substring-after($text,'&#xa;')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>