﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

	<xsl:template name="renderOffer">
		<xsl:if test="sc:fld('Title',.)">
        
			<div class="offer">
            
                <xsl:if test="string-length(sc:fld('Image', ., 'src')) &gt; 0">
                <div class="offerImage">
                    <img alt="">
                         <xsl:attribute name="src">
                            <xsl:value-of select="sc:fld('Image', ., 'src')"></xsl:value-of>
                         </xsl:attribute>
                        <xsl:attribute name="alt">
                            <xsl:value-of select="sc:fld('Title',.)"/>
                        </xsl:attribute>
                    </img>
                </div>
                </xsl:if>
                
				<div class="offerBg">
					<sc:link field="Hyperlink">
						<img class="offerArrow" alt="" src="/img/explore-icon.png" />
					</sc:link>
					<div class="offerDetails">
						<sc:link field="Hyperlink">
							<sc:text field="Title" />
						</sc:link>
					</div>
					<xsl:choose>
						<xsl:when test="sc:fld('Start Date',.)">
                        
                            <div class="offerContainer">
                                <div class="offerDates">
                                    <div class="startDateCalendar">
                                        <div class="startMonth"><xsl:value-of select="sc:formatdate(sc:fld('Start Date', .), 'MMM')" /></div>
                                        <div class="startDate"><xsl:value-of select="substring-after(sc:formatdate(sc:fld('Start Date', .), '/d'), '/')" /></div>
                                    </div>
                                    <xsl:if test="sc:fld('End Date',.)">
                                        <div class="dash">-</div>
                                        <div class="endDateCalendar">
                                            <div class="endMonth"><xsl:value-of select="sc:formatdate(sc:fld('End Date', .), 'MMM')" /></div>
                                            <div class="endDate"><xsl:value-of select="substring-after(sc:formatdate(sc:fld('End Date', .), '/d'), '/')" /></div>
                                        </div>
                                    </xsl:if>
                                </div>
                                <div class="offerDescription">
                                    <sc:text field="Description" />
                                </div>
                            </div>
						</xsl:when>
						<xsl:otherwise>
							<div class="offerDescription wide">
								<sc:text field="Description" />
							</div>
						</xsl:otherwise>
					</xsl:choose>



				</div>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
