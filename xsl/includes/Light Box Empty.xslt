﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<!--==============================================================-->
	<!-- Lightbox Image                                          -->
	<!--==============================================================-->
	<xsl:template name="Light-Box-Empty">
		<img src="/img/blank.gif" width="214" height="159" />
	</xsl:template>

</xsl:stylesheet>