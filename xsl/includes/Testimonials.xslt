﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<xsl:template name="renderTestimonials">
		<xsl:if test="sc:fld('Testimonials',.)">
			<div class="testimonials">
				<div id="carousel-testimonial" class="wrap-carousel-text">
					<ul class="carousel">
						<xsl:for-each select="sc:Split('Testimonials',.)">
							<xsl:for-each select="sc:item(text(),.)">
								<xsl:if test="sc:fld('Quote',.)">
									<li>
                                    	<div>
                                            <img src="/img/icon-silhouette-dark.png" alt="Silhouette of a persons head" class="testimonial-silhouette">
                                                <xsl:if test="sc:fld('Person Details',.)">
                                                    <xsl:attribute name="alt">
                                                        <xsl:value-of select="sc:fld('Person Details',.)"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="title">
                                                        <xsl:value-of select="sc:fld('Person Details',.)"/>
                                                    </xsl:attribute>
                                                </xsl:if>
                                            </img>
                                            <div class="testimonial-text"><sc:text field="Quote"/></div>
                                            <div class="user"><xsl:value-of select="sc:fld('Person Details',.)"/></div>
                                        </div>
									</li>
								</xsl:if>
								
							</xsl:for-each>
						</xsl:for-each>
					</ul>
                    <!--
					<a href="#" class="carousel-prev">
						<span>prev</span>
					</a>
					<a href="#" class="carousel-next">
						<span>next</span>
					</a>
                    -->
				</div>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
