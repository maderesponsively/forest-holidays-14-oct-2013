﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<xsl:template name="renderButtons">
		<xsl:if test="sc:fld('Buttons',.)">
			<div class="buttons">
				<xsl:for-each select="sc:Split('Buttons',.)">
					<xsl:for-each select="sc:item(text(),.)">
						<xsl:choose>
							<xsl:when test="sc:fld('Button Check Availability',.) = '1'">
								<a href="#" id="btnOpenAvailability">
									<div>
										<xsl:attribute name="class"><xsl:value-of select="sc:fld('Button Type',.)"/></xsl:attribute>
										<div class="label"><sc:text field="Button Text"/></div>
										<img src="/img/btn-arrow-right.png" />
									</div>
								</a>
								<!-- If this is an Availability Check button then open the Availability Search and scroll the page to the top -->
								<script type="text/javascript">
									$(document).ready(function() {
										$("#btnOpenAvailability").click(function(e) {
											$("#searchForm").css('display', 'block');
											$("#availability img").attr('src', '/img/availability-icon-up.png');
											$("html, body").animate({ scrollTop: 0 }, 'slow');
											e.preventDefault();
										});
									});
								</script>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="sc:fld('Button Text',.) and sc:fld('Button Link',.) ">
									<sc:link field="Button Link">
										<div>
											<xsl:attribute name="class"><xsl:value-of select="sc:fld('Button Type',.)"/></xsl:attribute>
											<div class="label"><sc:text field="Button Text"/></div>
											<img src="/img/btn-arrow-right.png" />
										</div>
									</sc:link>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
						
					</xsl:for-each>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
