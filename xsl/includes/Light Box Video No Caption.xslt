﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	
<!--==============================================================-->
<!-- Lightbox Video (No Caption beneath video)                    -->
<!--==============================================================-->

<xsl:template name="Light-Box-Video-No-Caption">
	<xsl:param name="index" />
	<xsl:param name="pos" />

	<div class="item">
		<xsl:if test="$pos > 4">
			<xsl:attribute name="class">item hidden</xsl:attribute>
		</xsl:if>
		
		<div class="play">
			<a class="fancybox iframe">
				<xsl:attribute name="rel">
					<xsl:choose>
						<xsl:when test="sc:fld('Gallery',.) = '1'">
							<!--DO NOT be part of a gallery-->
							<xsl:value-of select="concat(concat('prettyPhoto[galleryA',$pos),']')"/>
							<!--Video has unique gallery ID-->
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$index"/>
							<!--Normal Index, part of the gallery-->
							<xsl:value-of select="concat(concat('prettyPhoto[galleryA',$index),']')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
					<xsl:text>?iframe=true&#38;width=</xsl:text>
					<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
					<xsl:text>&#38;height=</xsl:text>
					<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
				</xsl:attribute>
				<xsl:if test="sc:fld('Caption',.)">
					<xsl:attribute name="title">
						<xsl:value-of select="sc:fld('Caption',.)"/>
						<xsl:if test="sc:fld('Submitted By',.)">
							, <xsl:text>submitted by </xsl:text><xsl:value-of select="sc:fld('Submitted By',.)"/>
						</xsl:if>
					</xsl:attribute>
				</xsl:if>
				<img alt="" src="/img/video-play-icon.png"></img>
			</a>
		</div>
		<a class="fancybox iframe">
			<xsl:attribute name="rel">
				<xsl:choose>
					<xsl:when test="sc:fld('Gallery',.) = '1'">
						<!--DO NOT be part of a gallery-->
						<xsl:value-of select="concat(concat('prettyPhoto[galleryB',$pos),']')"/>
						<!--Video has unique gallery ID-->
					</xsl:when>
					<xsl:otherwise>
						<!--Normal Index, part of the gallery-->
						<xsl:value-of select="concat(concat('prettyPhoto[galleryB',$index),']')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="href">
				<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
				<xsl:text>?iframe=true&#38;width=</xsl:text>
				<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
				<xsl:text>&#38;height=</xsl:text>
				<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
			</xsl:attribute>
			<xsl:if test="sc:fld('Caption',.)">
				<xsl:attribute name="title">
					<xsl:value-of select="sc:fld('Caption',.)"/>
					<xsl:if test="sc:fld('Submitted By',.)">
						, <xsl:text>submitted by </xsl:text><xsl:value-of select="sc:fld('Submitted By',.)"/>
					</xsl:if>
				</xsl:attribute>
			</xsl:if>
			<img alt="">
				<xsl:attribute name="src">
					<xsl:value-of select="sc:fld('Thumbnail Image',.,'src')"></xsl:value-of>
					<xsl:text>?w=214&#38;h=159</xsl:text>
				</xsl:attribute>
			</img>
		</a>
		<xsl:if test="sc:field('Caption',.)">
			<div class="caption">
				<p>
					<a>
						<xsl:attribute name="href">
							<xsl:value-of select="sc:fld('Video URL',.,'url')"></xsl:value-of>
							<xsl:text>?iframe=true&#38;width=</xsl:text>
							<xsl:value-of select="sc:fld('Width',.)"></xsl:value-of>
							<xsl:text>&#38;height=</xsl:text>
							<xsl:value-of select="sc:fld('Height',.)"></xsl:value-of>
						</xsl:attribute>
						<sc:text field="Caption"></sc:text>
					</a>
				</p>
			</div>
		</xsl:if>
	</div>
</xsl:template>

</xsl:stylesheet>