﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

	<xsl:template name="renderSlideshow">
		<xsl:param name="index" />
		<xsl:if test="sc:fld('Slideshow Images',.)">

			
			<div class="box">
				<div class="wrap wrap-carousel">
					<div class="small-carousel">
						<xsl:attribute name="id">
							<xsl:value-of select="concat('carousel', $index)"/>
							<!-- give the carousel a unique ID -->
						</xsl:attribute>
						<xsl:text>&#10;</xsl:text>
						<ul class="carousel">
							<xsl:for-each select="sc:Split('Slideshow Images',.)">
								<xsl:for-each select="sc:item(text(),.)">
									<xsl:variable name="alt"><xsl:value-of select="sc:fld('alt',.)" /></xsl:variable>
									<li>
										<img src="{sc:GetMediaUrl(.)}">
											<xsl:attribute name="title">
												<xsl:value-of select="$alt"/>
											</xsl:attribute>
										</img>
									</li>
								</xsl:for-each>
							</xsl:for-each>
						</ul>
						<a href="#" class="carousel-prev">
							<span>prev</span>
						</a>
						<a href="#" class="carousel-next">
							<span>next</span>
						</a>
						<div class="controls">
							<div class="pages"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- Render the js with dynamic carousel ID -->
            <!--
			<script type="text/javascript">
				<![CDATA[$("#]]><xsl:value-of select="concat('carousel', $index)"/><![CDATA[ .carousel").rcarousel({
				visible: 1,
				step: 1,
				speed: 500,
				auto: {
				enabled: true
				},
				width: 460,
				height: 325,
				start: initializeCarousel,
				pageLoaded: pageLoaded
				});]]>
			</script>
            -->
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
