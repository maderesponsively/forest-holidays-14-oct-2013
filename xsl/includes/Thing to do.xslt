﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  exclude-result-prefixes="sc">

	<!--==============================================================-->
	<!-- Thing to Do                                                  -->
	<!--==============================================================-->
	<xsl:template name="Things-to-Do">
		<xsl:param name="pos" />
		<div class="item">
			<xsl:if test="$pos mod 3 = 0">
				<xsl:attribute name="class">item last</xsl:attribute>
			</xsl:if>
			<sc:link field="Link">
			<sc:image field="Image" w="294" h="221"></sc:image>
				<xsl:if test="sc:fld('Caption',.)">
					<div class="caption">
						<sc:text field="Caption" show-title-when-blank="true" />
						<div class="explore"></div>
					</div>
				</xsl:if>
			</sc:link>
		</div>
	</xsl:template>

</xsl:stylesheet>