﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
	File: Location Map.xslt                                                   
	Created by: sitecore\paulh                                       
	Created: 10/09/2012 09:57:34                                               
	Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- Includes -->
<xsl:include href="includes/Functions.xslt"/>	
	
<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
	
<xsl:template match="*" mode="main">
	<xsl:text>&#10;&#10;</xsl:text>
	<xsl:comment>Location Map Start</xsl:comment>
	<xsl:text>&#10;</xsl:text>
	<script type="text/javascript" src="/js/locationmap.js"></script>
	<div id="locationMap">
		<div id="regions">
			<xsl:variable name="LocationsRootItem" select="sc:item(fh:ConfigValue('LocationsItemId'),.)"></xsl:variable>
			<p><xsl:value-of select="sc:fld('Regions Title',$LocationsRootItem)" /></p>
			<div id="dropDown">
				<a href="#" class="dropDown">
					<span><xsl:value-of select="sc:fld('Regions DropDown Text',$LocationsRootItem)" /></span>
					<img src="/img/filter-arrow-up.png" alt="Up" />
				</a>
				<div id="regionList">
					<xsl:for-each select="sc:item(fh:ConfigValue('RegionsItemId'),.)/item">
						<div class="region">
							<sc:link field="Region Url">
									<xsl:value-of select="sc:fld('Region Name',.)" />
							</sc:link>
						</div>
					</xsl:for-each>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			<![CDATA[$(document).ready(function () {
				$("#regions #dropDown a.dropDown img").attr("src", "/img/filter-arrow-down.png");
				$('#regions #dropDown a.dropDown').toggle(function () {
					$("#regions #dropDown a.dropDown img").attr("src", "/img/filter-arrow-up.png");
					$('#regions #dropDown #regionList').slideDown('fast');
					}, function () {
					$("#regions #dropDown a.dropDown img").attr("src", "/img/filter-arrow-down.png");
					$('#regions #dropDown #regionList').slideUp('fast');
				});
			});
			]]>
		</script>
		<div id="map">
			<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
				<xsl:if test="sc:fld('Show on Map', .)">
					<xsl:text>&#10;&#09;&#09;</xsl:text>
					<div>
						<xsl:attribute name="class">
							<xsl:text>flag </xsl:text>
							<xsl:call-template name="ToLower">
								<xsl:with-param name="inputString" select="translate(sc:fld('Location Name',.),' ','')" />
							</xsl:call-template>
						</xsl:attribute>
						<sc:link field="Location Url">
							<span>
								<xsl:value-of select="sc:fld('Location Name',.)" />
							</span>
						</sc:link>
					</div>
				</xsl:if>
			</xsl:for-each>

			<xsl:text>&#10;&#09;</xsl:text>
			<xsl:for-each select="sc:item(fh:ConfigValue('LocationsItemId'),.)/item">
				<xsl:if test="sc:fld('Show on Map', .)">
					<xsl:text>&#10;&#09;&#09;</xsl:text>
					<div>
						<xsl:attribute name="class">
							<xsl:call-template name="ToLower">
								<xsl:with-param name="inputString" select="translate(sc:fld('Location Name',.),' ','')" />
							</xsl:call-template>
							<xsl:text>Box</xsl:text>
						</xsl:attribute>
						<xsl:text>&#10;&#09;&#09;&#09;</xsl:text>
						<div class="boxContent">
							<xsl:text>&#10;&#09;&#09;&#09;&#09;</xsl:text>
							<sc:link field="Location Url">
								<sc:image field="Lightbox Image"></sc:image>
							</sc:link>
							<div class="content">
								<p><sc:text field="Lightbox Content" /></p>
							</div>
							<xsl:if test="sc:fld('Activities',.)">
								<ul class="icons">
									<xsl:for-each select="sc:Split('Activities',.)">
										<xsl:if test="position() &lt;= 8">
											<xsl:for-each select="sc:item(text(),.)">
												<xsl:variable name="activityName" select="sc:fld('Activity Name',.)"></xsl:variable>
												<li>
													<sc:image field="Activity Icon" Title="{$activityName}"></sc:image>
												</li>
											</xsl:for-each>
										</xsl:if>
									</xsl:for-each>
								</ul>
							</xsl:if>
						</div>
						<xsl:comment>/.boxContent</xsl:comment>
						<xsl:text>&#10;&#09;&#09;</xsl:text>
					</div>
				</xsl:if>
			</xsl:for-each>
		</div>
	</div>
	
</xsl:template>

</xsl:stylesheet>