﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
	File: Content Blocks.xslt                                                   
	Created by: sitecore\paulh                                       
	Created: 16/08/2012 15:20:22                                               
	Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">


	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8" />

	<!-- parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>

	<!-- entry point -->
	<xsl:template match="*">
		<xsl:apply-templates select="$sc_item" mode="main"/>
	</xsl:template>

	<!--==============================================================-->
	<!-- main                                                         -->
	<!--==============================================================-->
	<xsl:template match="*" mode="main">

		<!-- Get Header Text Colour Item -->
		<xsl:variable name="colourItem" select="sc:item(sc:fld('Header Text Colour',.),.)" />

		<!-- Spotlight text etc. Only if 'Header Spotlight Text' is not empty -->
		<xsl:if test="sc:fld('Header Spotlight Text',.)">
			<div class="spotlight">
				<h2>
					<sc:text field="Header Spotlight Text" />
				</h2>
				<xsl:if test="sc:fld('Header Text',.)">
					<xsl:variable name="colour" select="sc:fld('Hex Value',$colourItem)" />
					<h3>
						<xsl:if test="$colour">
							<xsl:attribute name="style">
								<xsl:value-of select="concat('color: #',$colour)" />
								<xsl:text>;</xsl:text>
							</xsl:attribute>
						</xsl:if>
						<sc:text field="Header Text" />
					</h3>
					<hr>
						<xsl:if test="$colour">
							<xsl:attribute name="style">
								<xsl:value-of select="concat(concat(concat('color: #',$colour),'; background-color: #'), $colour)" />
								<xsl:text>;</xsl:text>
							</xsl:attribute>
						</xsl:if>
					</hr>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>


</xsl:stylesheet>
