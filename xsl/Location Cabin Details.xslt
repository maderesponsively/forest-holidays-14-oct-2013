﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Location Cabin Details.xslt                                                   
    Created by: sitecore\jamesm                                       
    Created: 08/10/2012 13:01:29                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  xmlns:fh="http://www.sitecore.net/xslext"
  exclude-result-prefixes="dot sc fh">

<!-- Includes -->
<xsl:include href="includes/Light Box Image.xslt"/>
<xsl:include href="includes/Light Box Video No Caption.xslt"/>
<xsl:include href="includes/Buttons.xslt"/>
<xsl:include href="includes/Slideshow.xslt"/>
<xsl:include href="includes/Testimonials.xslt"/>
<xsl:include href="includes/In the Media.xslt"/>
<xsl:include href="includes/Additional Information.xslt"/>
	
<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!--<xsl:variable name="siteSettings" select="sc:item(fh:ConfigValue('SiteSettingItemId'),.)" />-->

<!-- entry point -->
<xsl:template match="*">
  <xsl:apply-templates select="$sc_item" mode="main"/>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
	<xsl:if test="sc:fld('Content Blocks',.)">
		<div id="contentBlockContainer">
			<xsl:for-each select="sc:Split('Content Blocks',.)">
				<xsl:variable name="counter" select="position()" />
				<xsl:for-each select="sc:item(text(),.)">

					<!-- Get Header Text Colour Item. Also used to set hr colour -->
					<xsl:variable name="colourItem" select="sc:item(sc:fld('Header Text Colour',.),.)" />
					<xsl:variable name="colour" select="sc:fld('Hex Value',$colourItem)" />

					<!--Let's style the hr tags-->
					<style type="text/css">
						.contentBlock .contentContainer.location hr 
						{
						color: #<xsl:value-of select="$colour" />;
						background-color: #<xsl:value-of select="$colour" />;
						}
					</style>
					
					<div class="contentBlock bg">
						<!-- default class. Applies a min-height-->
						<xsl:choose>
							<xsl:when test="sc:fld('Background Image',.)">
								<xsl:attribute name="style">
									<xsl:text>background-image: url(</xsl:text>
									<xsl:value-of select="sc:fld('Background Image', ., 'src')" />
									<xsl:text>);</xsl:text>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<!-- No background image so no min-height -->
								<xsl:attribute name="class">contentBlock</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>

						<div class="contentContainer location">
							<!-- 'Overview' Text -->
							<xsl:if test="sc:fld('Overview',.)">
								<div class="cabinOverview">
									<xsl:attribute name="style">
										<xsl:text>background-image: url(</xsl:text>
										<xsl:value-of select="sc:fld('Overview Background', ., 'src')" />
										<xsl:text>)</xsl:text>
									</xsl:attribute>
									<div class="bodyText">
										<sc:html field="Overview" />
									</div>
								</div>
							</xsl:if>
							
							<!-- 'Features' Icons -->
							<xsl:if test="sc:fld('Features',.)">
								<hr />
								<div id="features">
									<div class="container">
										<xsl:for-each select="sc:Split('Features',.)">
											<xsl:for-each select="sc:item(text(),.)">
												<div class="feature">
													<xsl:attribute name="style">
														<xsl:text>background-image: url(</xsl:text>
														<xsl:value-of select="sc:fld('Image', ., 'src')" />
														<xsl:text>);background-repeat: no-repeat;</xsl:text>
													</xsl:attribute>
													<p>
														<sc:text field="Description" />
													</p>
												</div>
											</xsl:for-each>
										</xsl:for-each>
									</div>
								</div>
							</xsl:if>
							
							<!-- 'Images Videos' thumbnails -->
							<xsl:if test="sc:fld('Images Videos',.)">
								<div class="video thumbnails">
									<xsl:for-each select="sc:Split('Images Videos',.)">
										<xsl:variable name="pos" select="position()" />
										<xsl:for-each select="sc:item(text(),.)">
											<xsl:choose>
												<xsl:when test="sc:ToLower(@template) ='image light box'">
													<xsl:choose>
														<xsl:when test="sc:fld('Disable lightbox',.) = '1'">
															<div class="item">
																<sc:image field="Image" w="214" h="159"></sc:image>
																<xsl:if test="sc:field('Caption',.)">
																	<div class="caption">
																		<p>
																			<sc:text field="Caption"></sc:text>
																		</p>
																	</div>
																</xsl:if>
															</div>
														</xsl:when>
														<xsl:otherwise>
															<xsl:call-template name="Light-Box-Image">
																<xsl:with-param name="index" select="$counter" />
																<xsl:with-param name="pos" select="$pos" />
															</xsl:call-template>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:when>
												<xsl:when test="sc:ToLower(@template) ='video light box'">
													<xsl:call-template name="Light-Box-Video-No-Caption">
														<xsl:with-param name="index" select="$counter" />
														<xsl:with-param name="pos" select="$pos" />
													</xsl:call-template>
												</xsl:when>
											</xsl:choose>
										</xsl:for-each>
									</xsl:for-each>
								</div>
							</xsl:if>
						
							<!-- Spotlight text etc. Only if 'Header Spotlight Text' is not empty -->
							<xsl:if test="sc:fld('Header Spotlight Text',.)">
								<div class="spotlight">
									<h2>
										<sc:text field="Header Spotlight Text" />
									</h2>
									<xsl:if test="sc:fld('Header Text',.)">
										
										<h3>
											<xsl:if test="$colour">
												<xsl:attribute name="style">
													<xsl:value-of select="concat('color: #',$colour)" />
													<xsl:text>;</xsl:text>
												</xsl:attribute>
											</xsl:if>
											<sc:text field="Header Text" />
										</h3>
										<hr />
									</xsl:if>
								</div>
							</xsl:if>
								
							<!-- 'Cabin Details' HTML -->
							<div id="cabinDetails">
								<sc:html field="Cabin Details" />
							</div>
							
							<!-- Slideshow (RHS) -->
							<div class="Right">
								<xsl:call-template name="renderSlideshow">
									<xsl:with-param name="index" select="43" />
								</xsl:call-template>
							</div>
							
							<!-- Testimonials (RHS) -->
							<div class="Right">
								<xsl:call-template name="renderTestimonials"/>
								<xsl:call-template name="inTheMedia" />
								<xsl:call-template name="additionalInformation" />
							</div>							
							
							<!-- Buttons -->
							<div class="Right">
								<xsl:call-template name="renderButtons"/>
							</div>
						</div>
					</div>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>