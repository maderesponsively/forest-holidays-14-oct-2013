﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Simple Layout.aspx.cs" Inherits="FHWeb2012.layouts.Simple_Layout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <%-- 
    <!-- Google Tag Manager 
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-F7QK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-F7QK');</script>
     End Google Tag Manager --> 
     --%>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
             <sc:Placeholder ID="Placeholder1" runat="server" Key="mainContent"></sc:Placeholder>
        </div>
    </form>
</body>
</html>
