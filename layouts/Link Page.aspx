﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Link Page.aspx.cs" Inherits="FHWeb2012.layouts.Link_Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <sc:Placeholder ID="Placeholder1" runat="server" Key="headContainer"></sc:Placeholder>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        .style1
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-F7QK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-F7QK');</script>
    <!-- End Google Tag Manager --> 

    <asp:Literal runat="server" ID="litGoogleCode" />
    <form id="form1" runat="server">
    <div>
        <img src="/img/logo.png" style="border: none;" alt="Forest Holidays Logo" /></div>
    <p class="style1">
        The requested page has been moved, please wait while we redirect you.</p>
    <p class="style1">
        If you are not automatically redirected within 5 seconds, please
        <asp:HyperLink runat="server" ID="hypLink">click here</asp:HyperLink>.</p>
    </form>
</body>
</html>
