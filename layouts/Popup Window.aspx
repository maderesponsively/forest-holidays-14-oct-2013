﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Popup Window.aspx.cs" Inherits="FHWeb2012.layouts.Popup_Window" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <sc:Placeholder ID="Placeholder1" runat="server" Key="headTags"></sc:Placeholder>
    <meta http-equiv="X-UA-Compatible" content="IE=8,chrome=1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/bmp" />
    <link rel="stylesheet" href="/css/popup.css" type="text/css" media="screen" />
</head>
<body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-F7QK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-F7QK');</script>
    <!-- End Google Tag Manager --> 


    <asp:Literal runat="server" ID="litGoogleBodyCode" />
    <form id="form1" runat="server">
        <div id="popup"><sc:Placeholder ID="phContentBlocks" runat="server" Key="contentBlocks"></sc:Placeholder></div>
    </form>
</body>
</html>
