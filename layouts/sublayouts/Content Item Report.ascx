﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Content Item Report.ascx.cs"
    Inherits="FHWeb2012.layouts.sublayouts.Content_Item_Report" %>
<div style="float: left; clear: both;">
    <br />
    <p>
        Page&nbsp;Count:&nbsp;<asp:Literal runat="server" ID="litPageCount"></asp:Literal></p>
    <br />
    <p>
        'Ready&nbsp;for&nbsp;Go&nbsp;Live'&nbsp;Count:&nbsp;<asp:Literal runat="server" ID="litReadyForGoLiveCount"></asp:Literal></p>
    <br />
    <asp:Button Visible="true" ID="btnExcel" runat="server" Text="Export to Excel" OnClick="Button1_Click1">
    </asp:Button>
</div>
<div style="float: left;">
    <asp:GridView ID="GV_SiteNodes" runat="server" AutoGenerateColumns="false" Font-Names="Verdana"
        Font-Size="Small" HeaderStyle-BackColor="silver" HeaderStyle-Font-Bold="true"
        AllowSorting="false">
        <RowStyle BackColor="#ffffff" />
        <Columns>
            <asp:BoundField DataField="GUID" HeaderText="ID" Visible="false">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ReadyForGoLive" HeaderText="Ready for Go Live?">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="NavigationTitle" HeaderText="Navigation Title">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="BodyTitle" HeaderText="Body Title">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="URL" HeaderText="URL">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="TemplateName" HeaderText="Template">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="PageTitle" HeaderText="Page Title">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="Keywords" HeaderText="Meta Keywords">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Description">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="Link" HeaderText="Link URL">
                <ItemStyle Wrap="False" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
</div>
