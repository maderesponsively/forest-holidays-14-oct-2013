﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Secure Payment Layout.aspx.cs" Inherits="FHWeb2012.layouts.Secure_Payment_Layout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <sc:Placeholder ID="Placeholder1" runat="server" Key="headTags"></sc:Placeholder>
    <!-- Invalid in HTML5 - Ideally will be added directly into the HTTP headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=8,chrome=1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/png" />
    <link rel="apple-touch-icon-precomposed" href="/img/apple-touch.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/apple-touch-ipad.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/apple-touch-retina.png" />
    <link rel="stylesheet" href="/css/rcarousel.css?v=5.9.13" type="text/css" media="screen" />
    <link rel="stylesheet" href="/css/prettyPhoto.css?v=5.9.13" type="text/css" media="screen" />
    <link rel="stylesheet" href="/css/forest-main.css?v=5.9.13" type="text/css" media="screen" />
    <link rel="stylesheet" href="/css/styles.css?v=5.9.13" type="text/css" media="screen" />

    <asp:Literal runat="server" ID="litSearchFormCSS" />

    <link rel="stylesheet" href="/css/media-queries.css?v=5.9.13" type="text/css" media="screen" /><!-- new -->
    <link rel="stylesheet" href="/css/clearfix.css?v=5.9.13" type="text/css" media="screen" /><!-- new -->
    <link rel="stylesheet" href="/css/jquery.flexslider.css?v=5.9.13" type="text/css" media="screen" /><!-- new -->

    <link rel="stylesheet" href="/css/jquery.selectboxit.css?v=17.9.13" type="text/css" media="screen" /><!-- New search styles -->

    <!-- FancyBox -->
    <link rel="stylesheet" href="/css/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" /><!-- new -->
    <link rel="stylesheet" href="/css/fancybox/jquery.fancybox-buttons.css?v=2.1.5" type="text/css" media="screen" /><!-- new -->
    <link rel="stylesheet" href="/css/fancybox/jquery.fancybox-thumbs.css?v=2.1.5" type="text/css" media="screen" /><!-- new -->

    <!--[if lte IE 8]>
        <link rel="stylesheet" href="/css/forest-main.ie8.css" type="text/css" media="screen" />
        <![endif]-->
    <!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/forest-main.ie7.css" type="text/css" media="screen" />
        <![endif]-->
    <style type="text/css">
        /* inline selection of rCarousel navigation bar background colour */
        #hero #controls
        {
            background-color: rgba(0, 0, 0, 0.4);
        }
    </style>
    
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/html5shiv.js"></script>
    <![endif]-->
    
    <!-- jQuery -->
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
	
	
	<script type="text/javascript" src="/js/modernizr.custom.js"></script>
    <script type="text/javascript" src="/js/forest-main.js?v=5.9.13"></script>
    <!-- Fancybox -->
    <script type="text/javascript" src="/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="/js/fancybox/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="/js/fancybox/jquery.fancybox-thumbs.js"></script>
    
    <!-- jQuery TouchWipe plugin -->
    <script type="text/javascript" src="/js/jquery.touchwipe.min.js"></script>
        
    <script type="text/javascript" src="/js/styles.js?v=5.9.13"></script>
    <script type="text/javascript" src="/js/jquery.ui.core.min.js"></script>
    <script type="text/javascript" src="/js/jquery.ui.widget.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fixed.js"></script>
    <script type="text/javascript" src="/js/jquery.ui.datepicker.min.js"></script>
    <script type="text/javascript" src="/js/jquery.tap.js"></script><!-- new -->
    
    <!-- rCarousel setup -->
    <script type="text/javascript" src="/js/jquery.flexslider.js?v=5.9.13"></script>
    <script type="text/javascript" src="/js/rcarousel.js?v=5913"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var autoScroll = true; // false to switch off auto scroll
            carouselSetup(autoScroll);
        });
    </script>
    <!-- end rCarousel setup -->

    <asp:Literal runat="server" ID="litSearchFormJS" />
    <asp:Literal runat="server" ID="litGoogleHeadCode" />
</head>
<body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-F7QK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-F7QK');</script>
    <!-- End Google Tag Manager --> 

    <asp:Literal runat="server" ID="litGoogleBodyCode" />
    <sc:Placeholder ID="Placeholder2" runat="server" Key="pageTracking"></sc:Placeholder>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlCookieMessage">
        <div id="cookieMessageContainer">
            <div id="cookieMessage">
                <asp:Literal runat="server" ID="litEUCookie" />
                <div id="cookieMessageClose">
                    <asp:LinkButton ID="lbClose" runat="server" OnClick="lbClose_Click">Close</asp:LinkButton></div>
            </div>
        </div>
    </asp:Panel>
    <div id="corpus">
        <sc:Placeholder ID="phPageHeader" runat="server" Key="pageHeader"></sc:Placeholder>
        <sc:Placeholder ID="phPageNavigation" runat="server" Key="pageNavigation"></sc:Placeholder>
        <asp:Panel ID="main" runat="server" clientidmode="Static">
            <sc:Placeholder ID="phAvailabilitySearch" runat="server" Key="availabilitySearch"></sc:Placeholder>
            <sc:Placeholder ID="phCarousel" runat="server" Key="carousel"></sc:Placeholder>
            <div class="layoutContainer"><sc:Placeholder ID="phMainContainer" runat="server" Key="mainContainer"></sc:Placeholder></div><!-- /.layoutContainer -->
            <sc:Placeholder ID="phContentBlocks" runat="server" Key="contentBlocks"></sc:Placeholder>   
            <sc:Placeholder ID="phFooter" runat="server" Key="footer"></sc:Placeholder>   
        </asp:Panel>
        <!-- /#main -->
        <sc:Placeholder ID="phStickyFooter" runat="server" Key="stickyFooter"></sc:Placeholder>
    </div>
    <asp:Literal runat="server" ID="litTracking" />
    </form>
</body>
</html>
