﻿jQuery(document).ready(function ($) {
	
	
	enquire.register("screen and (min-width:780px)", {
    match : function() {
    var position;     
    
    //Argyll
    $("#map .argyll a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .argyllBox").toggle();
    });
    $("#locationMap .argyllBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .argyllBox").show();
    }, function () {
        $("#locationMap .argyllBox").hide();
    });

    //Blackwood Forest
    $("#map .blackwoodforest a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .blackwoodforestBox").toggle();
    });
    $("#locationMap .blackwoodforestBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .blackwoodforestBox").show();
    }, function () {
        $("#locationMap .blackwoodforestBox").hide();
    });

    //Cropton
    $("#map .cropton a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .croptonBox").toggle();
    });

    $("#locationMap .croptonBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .croptonBox").show();
    }, function () {
        $("#locationMap .croptonBox").hide();
    });

    //Delamere Forest
    $("#map .delamereforest a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .delamereforestBox").toggle();
    });
    $("#locationMap .delamereforestBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .delamereforestBox").show();
    }, function () {
        $("#locationMap .delamereforestBox").hide();
    });

    //Deerpark
    $("#map .deerpark a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .deerparkBox").toggle();
    });
    $("#locationMap .deerparkBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .deerparkBox").show();
    }, function () {
        $("#locationMap .deerparkBox").hide();
    });

    //Forest of Dean
    $("#map .forestofdean a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .forestofdeanBox").toggle();
    });
    $("#locationMap .forestofdeanBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .forestofdeanBox").show();
    }, function () {
        $("#locationMap .forestofdeanBox").hide();
    });

    //Keldy
    $("#map .keldy a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .keldyBox").toggle();
    });
    $("#locationMap .keldyBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .keldyBox").show();
    }, function () {
        $("#locationMap .keldyBox").hide();
    });

    //Sherwood Forest
    $("#map .sherwoodforest a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .sherwoodforestBox").toggle();
    });
    $("#locationMap .sherwoodforestBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .sherwoodforestBox").show();
    }, function () {
        $("#locationMap .sherwoodforestBox").hide();
    });

    //Strathyre
    $("#map .strathyre a").hover(function (e) {
        e.preventDefault();
        $("#locationMap .strathyreBox").toggle();
    });
    $("#locationMap .strathyreBox").hover(function (e) {
        e.preventDefault();
        $("#locationMap .strathyreBox").show();
    }, function () {
        $("#locationMap .strathyreBox").hide();
    });

		},  
		unmatch : function() {
			
			$("#map .flag a").each(function() {
				$(this).unbind('mouseenter mouseleave');
			});

			$("#locationMap .flag").each(function() {
				$(this).unbind('mouseenter mouseleave');
			});

		}
	});
		   
});