window.publicFunctions = {};
//var slowScrollTargets = new Array(); //previous test
var programmaticallyScrolling = false;

$(document).ready(function () {
    $(".sticky a").click(function (event) {
        $(this).parents(".sticky").find("a").removeClass("active");
        $(this).addClass("active");

        //event.preventDefault();
    });

    //Watch scroll position
    $(window).scroll(function () {
        //$('#navigation-wrapper').clearQueue().stop().animate({"marginTop": $(window).scrollTop() + "px"}, "slow" );
        window.publicFunctions.findPage();
    });

    function isScrolledIntoView(elem) {
        //console.log(elem);
        var shift = 200; //just to move all boxes up in the check

        var docViewTop = ($(window).scrollTop()) + shift;
        var docViewBottom = docViewTop + $(window).height();

        //console.log($(elem).offset().top);
        if ((elem).offset()) {
            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
        }
        else {
            return null
        }
    }

    window.publicFunctions.findPage = function () {
        if (!programmaticallyScrolling) {
            var positionFound = false;

            //console.log('finding page...');

            $('.sticky a').each(function (index) {
                //console.log('each');
                var target = $(this).attr('href').split('#')[1]; //Always get the hash-tag id value...
                //console.log(target);

                if (isScrolledIntoView($('#' + target)) && !positionFound) {
                    //console.log(target);
                    //setNavIndicatorPosition($(this).context.offsetTop);
                    $(".sticky a").removeClass('active');
                    $(".sticky a[href*='" + target + "']").addClass('active');

                    positionFound = true;
                }
            });

            /*if(!positionFound) {
            console.log('not found');
            console.log($('div[id*=\'project-\']'));
            console.log(isScrolledIntoView($('div[id*=\'project-\']')));
            if(isScrolledIntoView($('div[id*=\'project-\']'))) {
            console.log('our work');
            setNavIndicatorPosition($('a[href*=#our-work]').context.offsetTop);
            positionFound = true;
            }
            }*/
        }
    }
});

/* Sadly the findPage has to be called here, not when the skeleton (document.ready) is finished
	but when the page has finished loading. This means I've had to fix the scope issue with publicFunctions. */
$(window).load(function() {
    window.publicFunctions.findPage();
});


// Smooth scrolling of Extras
$(document).ready(function () {
    $("#sub-nav-bar").localScroll({
        offset: -140
    });
});
