$(document).ready(function() {
    $('.easteregg').hover(function() {
        $(this).children('.normal').fadeOut(500);
        $(this).children('.hover').fadeIn(500);
    }, function() {
        $(this).children('.normal').fadeIn(500);
        $(this).children('.hover').fadeOut(500);
    });
});