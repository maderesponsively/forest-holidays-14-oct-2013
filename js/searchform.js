﻿// global variables for the search form tabs
var $tabLi, $tabLink, $tabDiv;

// Gets size of all columns and checks that middle column is
// always one away from the end
function midColumnRight(column) {
    var size = $("#searchDates ul > li").size() - 1;

    if (column >= size - 1) {
        return true;
    }
    return false;
}

// Checks that column is always one ahead of the first element
function midColumnLeft(column) {
    if (column <= 1) {
        return true;
    }
    return false;
}

var midColumn = 1;

// Remove non Mon & Fri dates depending on stay length indicated - JM
function hideInvalidDay(date) {
    var d = new Date();
    d.setHours(0, 0, 0, 0);    // Set to midnight
    if (date >= d) {
        var checkedRadioValue = $("input[name='availabilitysearch_0$rbDuration']:checked").val();
        //console.log(checkedRadioValue);
        var dayNumber = date.getDay().toString();
        if (dayNumber == 1 && (checkedRadioValue == 4 || checkedRadioValue == 7 || checkedRadioValue == 11 || checkedRadioValue == 14)) {
            return [true];
        }
        if (dayNumber == 5 && (checkedRadioValue == 3 || checkedRadioValue == 7 || checkedRadioValue == 10 || checkedRadioValue == 14)) {
            return [true];
        }
    }
    return [false];
}

function searchResultGrey() {
    $("#searchForm .flexible input").click(function () {
        if ($(".searchDetails").length > 0) {
            if (!$(this).is(":checked")) {
                $("#searchDates ul").addClass("inflexible");
                $(".searchDetails ul").addClass("inflexible");
            } else {
                $("#searchDates ul").removeClass("inflexible");
                $(".searchDetails ul").removeClass("inflexible");
            }
        }
    });
}

// Replicates click functionality on other elements in item
function cornerImage() {
    $(".item .corner a").click(function (e) {
        e.preventDefault();

        var currentItem = $(this).parent().parent();
        var itemAnchor = currentItem.find("> a");

        itemAnchor.click();
    });
    $(".item .caption p a").click(function (e) {
        e.preventDefault();

        var currentItem = $(this).parent().parent().parent();
        var itemAnchor = currentItem.find("> a");

        itemAnchor.click();
    });
}

// Shows/hides the search form info box
function infoBoxDisplay() {
    $(document).click(function () {
        $("#searchForm .infoLink").click(function (e) {
            e.stopPropagation();
        });

        $("#searchForm .infoBox").click(function (e) {
            e.stopPropagation();
        });

        if ($("#searchForm .infoBox").is(':visible')) {
            $("#searchForm .infoBox").hide();
        }
    });

    // open when the info link is clicked
    $("#searchForm .infoLink").click(function (e) {
        e.preventDefault();
        $("#searchForm .infoBox").show();
    });

    // close when the info box close button is clicked
    $("#searchForm .infoBox .greenButton").click(function (e) {
        e.preventDefault();
        $("#searchForm .infoBox").hide();
    });
}

// Selects/deselects all checkboxes on the search form map
function locationSelect() {

    // select all locations when select all link clicked
    $("#searchForm .select a:eq(0)").click(function (e) {
        $('#searchForm .map input:checkbox').attr('checked', 'checked');
        return false;
    });

    // de-select all locations when deselect all link clicked
    $("#searchForm .select a:eq(1)").click(function (e) {
        $('#searchForm .map input:checkbox').removeAttr('checked');
        return false;
    });
}

// Selects and highlights search form duration radio buttons
function durationSelect() {
    var radios = $("#searchForm .duration .radios input:radio");
    $(radios).click(function (e) {
        // uncheck all radios
        $(radios).attr('checked', false);
        // remove selected class from all radios
        $(this).parent().siblings().removeAttr("class");
        // check the current radio
        $(this).attr('checked', true);
        // add selected class to current radio
        $(this).parent().addClass("selected");
        // Change selected date in Textbox to next available date
        var checkedRadioValue = $(this).val();
        var d = new Date();
        if (checkedRadioValue == 3 || checkedRadioValue == 7 || checkedRadioValue == 10 || checkedRadioValue == 14)  // Friday
            d.setDate(d.getDate() + (12 - d.getDay()) % 7);
        else
            d.setDate(d.getDate() + (8 - d.getDay()) % 7);  // Next monday (or today if monday)
        $("#searchForm .date #hdntxtWhen").datepicker("setDate", d);
    });
}

// Set up the search form checkbox tabs
function tabInitialize(tab) {

    // hide the second tab content by default
    if (tab == 'cabin') {
        $("#searchForm .tabContent:eq(1)").hide();
        $("#searchForm .tabCabinType .type").addClass('active');
        $("#searchForm .tabCabinType .feature").removeClass('active');
    } else {
        $("#searchForm .tabContent:eq(0)").hide();
        $("#searchForm .tabCabinType .type").removeClass('active');
        $("#searchForm .tabCabinType .feature").addClass('active');
    }

    $tabLi = $("#searchForm .tabCabinType").find('li');
    $tabLink = $("p a", $tabLi);
    $tabDiv = $(".tabContent", $tabLi);

    $tabLink.click(function (e) {
        tabSwitch(this);
        e.preventDefault();
    });

    // Disable based on selection
    $("#chkDisabledAccess").click(function () {
        $("#chkHotTub").prop('checked', false);
        $("#chkHotTub").prop('disabled', ($("#chkDisabledAccess").is(':checked')) ? true : false);
    });
}

// Handles the search results day scrolling
function searchScroll() {
    var searchDates = $("#searchDates ul").children('li').eq(midColumn);
    searchDates.addClass("active");

    // add click event to later column
    $('#searchNav li.later a').click(function (event) {
        /*event.preventDefault();

        if (!midColumnRight(midColumn)) {
        midColumn += 1;

        $("#searchDates ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $('#searchDates').animate({
        scrollLeft: '+=118'
        }, 'fast');

        $("#searchDates ul").children('li').eq(midColumn).addClass("active");

        $(".searchDetails ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $(".searchDetails").each(function (e) {
        $(this).animate({
        scrollLeft: '+=118'
        }, 'fast');

        $(this).find("ul").children('li').eq(midColumn).addClass("active");
        });
        }*/
    });

    // add click event to previous column
    $('#searchNav li.earlier a').click(function (event) {
        /*event.preventDefault();

        if (!midColumnLeft(midColumn)) {
        midColumn -= 1;

        $("#searchDates ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $('#searchDates').animate({
        scrollLeft: '-=118'
        }, 'fast');

        $("#searchDates ul").children('li').eq(midColumn).addClass("active");

        $(".searchDetails ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $(".searchDetails").each(function (e) {
        $(this).animate({
        scrollLeft: '-=118'
        }, 'fast');

        $(this).find("ul").children('li').eq(midColumn).addClass("active");
        });
        }*/
    });
}

// Expands the search form and changes availability button icon
function searchFormExpand(o) {
    if (o.css("display") == "none") {
        $("#availability img").attr("src", "/img/availability-icon-up.png");
        o.slideDown("slow");
    } else {
        $("#availability img").attr("src", "/img/availability-icon-down.png");
        o.slideUp("slow");
    }
}

// Switches the search form checkbox tabs
function tabSwitch(o) {
    var clicked = $tabLink.index(o);
    var other = (clicked === 0) ? 1 : 0;
    // switch button on states    
    $($($tabLi)[clicked]).addClass('active');
    $($($tabLi)[other]).removeClass('active');
    // switch tab content shown
    $($($tabDiv)[clicked]).fadeIn('slow');
    $($($tabDiv)[other]).hide();
    // Untick the checkboxes on the hidden tab
    $($($tabDiv)[other]).find('input:checkbox').prop('checked', false);
}

$(document).ready(function () {
    $("#lblDelamere, #chkDelamere").hover(function (e) {
        e.preventDefault();
        $(".popup_delamere").stop(true, true).delay(100).show();
    }, function () {
        $(".popup_delamere").stop(true, true).delay(100).hide();
    });
});