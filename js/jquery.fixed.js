/***
 * 3Sixty appear after element plugin
 */
(function($){
    $.fn.fixElement = function(params){
        params = $.extend({
            appearAfterElement: 0
        }, params);
        
        var element = $(this);
        var distanceTop = 0;
        
        if(params.appearAfterElement) {
            distanceTop = element.offset().top + $(params.appearAfterElement).outerHeight(true) + element.outerHeight(true);
        } else {
            distanceTop = element.offset().top;
        }
        
        $(window).scroll(function() {
            if($(window).scrollTop() > (distanceTop - 260)) {
                element.css({
                    'position':'fixed',
                    'top':'0px',
                    'width': '100%'
                });
                //console.log($('#corpus>header'));
                $('#corpus>header').css('margin-bottom', '172px');
            } else {
                element.css({
                    'position':'static',
                    'float': 'left'
                });
                
                $('#corpus>header').css('margin-bottom', '0px');
            }
        });
    };
})(jQuery);