﻿// global variables for the search form tabs
var $tabLi, $tabLink, $tabDiv;

// Extend datePicker
$(function () {
    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
        afterShow.apply((inst.input ? inst.input[0] : null));
    }
});


function hideInvalidDay(date) {
    var d = new Date();
    d.setHours(0, 0, 0, 0);    // Set to midnight
    if (date >= d) {
        var checkedRadioValue = $("input[name*='rbDuration']:checked").val();
        var dayNumber = date.getDay().toString();
        // Check if any availability today, if not return false
        var dateNumber = date.getDate().toString();

        var splitDates = $("#hdnDatesWithAvailability").val().split(",");
        if (splitDates != null && splitDates.length > 1) {
            var found = false;
            for (i = 0; i < splitDates.length; i++) {
                var splitValues = splitDates[i].toString().split("|")
                if (splitValues[0].toString() == dateNumber.toString()) {
                    found = splitValues[1].toString() != '0';
                    break;
                }
            }

            if (!found) {
                return [false];
            }
        }

        if (dayNumber == 1 && (checkedRadioValue == 4 || checkedRadioValue == 7 || checkedRadioValue == 11 || checkedRadioValue == 14)) {
            return [true];
        }
        if (dayNumber == 5 && (checkedRadioValue == 3 || checkedRadioValue == 7 || checkedRadioValue == 10 || checkedRadioValue == 14)) {
            return [true];
        }
    }
    return [false];
}

function resultsHideInvalidDay(date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);    // Set to midnight
    if (date >= today) {
        var ddlCheckValue = $("#searchFormContainer .duration #availabilitysearch_0_ddlWhen option:selected").val();
        var dayNumber = date.getDay().toString();
        // Check if any availability today, if not return false
        var dateNumber = date.getDate().toString();

        var splitDates = $("#hdnDatesWithAvailability").val().split(",");
        if (splitDates != null && splitDates.length > 1) {
            var found = false;
            for (i = 0; i < splitDates.length; i++) {
                var splitValues = splitDates[i].toString().split("|")
                if (splitValues[0].toString() == dateNumber.toString()) {
                    found = splitValues[1].toString() != '0';
                    break;
                }
            }

            if (!found) {
                return [false, "", 'No cabins available'];
            }
        }

        if (dayNumber == 1 && (ddlCheckValue == 4 || ddlCheckValue == 7 || ddlCheckValue == 11 || ddlCheckValue == 14)) {
            return [true, "", splitValues[1].toString() + ' cabins available'];
        }
        if (dayNumber == 5 && (ddlCheckValue == 3 || ddlCheckValue == 7 || ddlCheckValue == 10 || ddlCheckValue == 14)) {
            return [true, "", splitValues[1].toString() + ' cabins available'];
        }
    }
    return [false];
}

// Replicates click functionality on other elements in item
function cornerImage() {
    $(".item .corner a").click(function (e) {
        e.preventDefault();

        var currentItem = $(this).parent().parent();
        var itemAnchor = currentItem.find("> a");

        itemAnchor.click();
    });
    $(".item .caption p a").click(function (e) {
        e.preventDefault();

        var currentItem = $(this).parent().parent().parent();
        var itemAnchor = currentItem.find("> a");

        itemAnchor.click();
    });
}

function locationSelect() {
    $("#btnSubmit").attr('disabled', 'disabled');
    var checkboxes = $("#searchFormContainer .map input:checkbox");
    $(checkboxes).click(function (e) {
        var count = $(".map input[type='checkbox']:checked").length;
        if (count > 0 && $("#hdntxtWhen").val() != '') {
            $("#btnSubmit").attr('enabled', true);
        }
        else {
            //alert(count + ', disabled');
            $("#btnSubmit").attr('enabled', false);
        };
    });
    $("#calendar").datepicker("refresh");
}

function postbackLocationSelect() {
    $("#btnSubmit").attr('disabled', 'disabled');
    var checkboxes = $("#searchFormContainer .map input:checkbox");
    $(checkboxes).click(function (e) {
        var count = $(".map input[type='checkbox']:checked").length;
        if (count > 0 && $("#hdntxtWhen").val() != '') {
            $("#btnSubmit").attr('enabled', true);
        }
        else {
            $("#btnSubmit").attr('disabled', 'disabled');
        };
    });
    $("#calendar").datepicker("refresh");
}

// Selects and highlights search form duration radio buttons
function durationSelect() {
    var d = new Date();
    var radios = $("#searchFormContainer .duration .radios input:radio");
    $(radios).click(function (e) {
        // uncheck all radios
        $(radios).attr('checked', false);
        // remove selected class from all radios
        $(this).parent().siblings().removeAttr("class");
        // check the current radio
        $(this).attr('checked', true);
        // add selected class to current radio
        $(this).parent().addClass("selected");
        // Change selected date in Textbox to next available date
        var checkedRadioValue = $(this).val();
        
        if (checkedRadioValue == 3 || checkedRadioValue == 7 || checkedRadioValue == 10 || checkedRadioValue == 14)  // Friday
            d.setDate(d.getDate() + (12 - d.getDay()) % 7);
        else
            d.setDate(d.getDate() + (8 - d.getDay()) % 7);  // Next monday (or today if monday)

        $("#calendar").datepicker("setDate", d);
    });
}

// Selects and highlights search form duration radio buttons
function postbackDurationSelect() {
    var d = new Date();
    var radios = $("#searchFormContainer .duration .radios input:radio");
    $(radios).click(function (e) {
        // uncheck all radios
        $(radios).attr('checked', false);
        // remove selected class from all radios
        $(this).parent().siblings().removeAttr("class");
        // check the current radio
        $(this).attr('checked', true);
        // add selected class to current radio
        $(this).parent().addClass("selected");
        // Change selected date in Textbox to next available date
        var checkedRadioValue = $(this).val();

        if (checkedRadioValue == 3 || checkedRadioValue == 7 || checkedRadioValue == 10 || checkedRadioValue == 14)  // Friday
            d.setDate(d.getDate() + (12 - d.getDay()) % 7);
        else
            d.setDate(d.getDate() + (8 - d.getDay()) % 7);  // Next monday (or today if monday)

        $("#calendar").datepicker("setDate", d);
        $("#calendar").datepicker("refresh");
    });
}


function resultsLocationSelect() {
    var checkboxes = $("#searchFormContainer .map input:checkbox");
    $(checkboxes).click(function (e) {
        var count = $(".map input[type='checkbox']:checked").length;
        if (count > 0 && $("#hdntxtWhen").val() != '') {
            $("#btnSubmit").attr('enabled', true);
        }
        else {
            $("#btnSubmit").attr('enabled', false);
        };
    });
    $("#calendar").datepicker("refresh");
}

function resultsPostbackLocationSelect() {
    var checkboxes = $("#searchFormContainer .map input:checkbox");
    $(checkboxes).click(function (e) {
        var count = $(".map input[type='checkbox']:checked").length;
        if (count > 0 && $("#hdntxtWhen").val() != '') {
            $("#btnSubmit").attr('enabled', true);
        }
        else {
            $("#btnSubmit").attr('enabled', false);
        };
    });
    $("#calendar").datepicker("refresh");
}

function resultsDurationDropdownSelect() {
    var ddl = $("#searchFormContainer .duration #availabilitysearch_0_ddlWhen");
    $(ddl).change(function () {
        $("#btnSubmit").attr('disabled', 'disabled');
        $("#calendar").datepicker("refresh");
    });
}

function resultsPostbackDurationDropdownSelect() {
    var ddl = $("#searchFormContainer .duration #availabilitysearch_0_ddlWhen");
    $(ddl).change(function () {
        $("#btnSubmit").attr('disabled', 'disabled');
        $("#calendar").datepicker("refresh");
    });
}


// Handles the search results day scrolling
function searchScroll() {
    var searchDates = $("#searchDates ul").children('li').eq(midColumn);
    searchDates.addClass("active");

    // add click event to later column
    $('#searchNav li.later a').click(function (event) {
        /*event.preventDefault();

        if (!midColumnRight(midColumn)) {
        midColumn += 1;

        $("#searchDates ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $('#searchDates').animate({
        scrollLeft: '+=118'
        }, 'fast');

        $("#searchDates ul").children('li').eq(midColumn).addClass("active");

        $(".searchDetails ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $(".searchDetails").each(function (e) {
        $(this).animate({
        scrollLeft: '+=118'
        }, 'fast');

        $(this).find("ul").children('li').eq(midColumn).addClass("active");
        });
        }*/
    });

    // add click event to previous column
    $('#searchNav li.earlier a').click(function (event) {
        /*event.preventDefault();

        if (!midColumnLeft(midColumn)) {
        midColumn -= 1;

        $("#searchDates ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $('#searchDates').animate({
        scrollLeft: '-=118'
        }, 'fast');

        $("#searchDates ul").children('li').eq(midColumn).addClass("active");

        $(".searchDetails ul li").each(function (e) {
        $(this).removeClass("active");
        });

        $(".searchDetails").each(function (e) {
        $(this).animate({
        scrollLeft: '-=118'
        }, 'fast');

        $(this).find("ul").children('li').eq(midColumn).addClass("active");
        });
        }*/
    });
}


function setupResultsAvailability() {
    var hidMap = $('#mapContainer');
    $("#mapDropDown").tap(50, function () {
        if (hidMap.is(":hidden")) {
            hidMap.slideDown();
        }
        else if (hidMap.is(":visible")) {
            hidMap.slideUp();
        }
    });

    $('.bedTrigger').each(function () {

        var bedConf = $(this).closest('.result').find('.rooms');

        $(this).tap(50, function () {
            if (bedConf.is(":hidden")) {
                bedConf.slideDown();
            }
            else if (bedConf.is(":visible")) {
                bedConf.slideUp();
            }
        });
    });


    $.fn.menuEdit = function (option1) {

        var hideBlock = $(option1);

        $(this).tap(50, function () {
            if (hideBlock.is(":hidden")) {
                hideBlock.slideDown();
                $(this).html($(this).html().replace('Edit', 'Close'));
            }
            else if (hideBlock.is(":visible")) {
                hideBlock.slideUp();
                $(this).html($(this).html().replace('Close', 'Edit'));
            }
        });
    }

    $("#locationTrigger").menuEdit('.block.location');
    $("#durationTrigger").menuEdit('.block.duration');
    $("#whenTrigger").menuEdit('.block.when');
    $("#tabsTrigger").menuEdit('.tab-container');
}

function setMobEditLocation(text) {
    $("#mobEditLocation span").text(text.toString());
}

function setMobEditDuration(text) {
    $("#mobEditDuration span").text(text.toString());
}

function setMobEditWhen(text) {
    $("#mobEditWhen span").text(text.toString());
}

function setMobEditTabs(text) {

}

$(document).ready(function () {
    $("#lblDelamere, #chkDelamere").hover(function (e) {
        e.preventDefault();
        $(".popup_delamere").stop(true, true).delay(100).show();
    }, function () {
        $(".popup_delamere").stop(true, true).delay(100).hide();
    });
});