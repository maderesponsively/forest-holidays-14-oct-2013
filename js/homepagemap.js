﻿jQuery(document).ready(function ($) {
	
	enquire.register("screen and (min-width:980px)", {
    match : function() {
    $("#homeMap .argyll a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .argyllBox").toggle();
    });
    $("#homeMap .argyllBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .argyllBox").show();
    }, function () {
        $("#homeMap .argyllBox").hide();
    });

    $("#homeMap .blackwoodforest a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .blackwoodforestBox").toggle();
    });
    $("#homeMap .blackwoodforestBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .blackwoodforestBox").show();
    }, function () {
        $("#homeMap .blackwoodforestBox").hide();
    });

    $("#homeMap .cropton a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .croptonBox").toggle();
    });
    $("#homeMap .croptonBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .croptonBox").show();
    }, function () {
        $("#homeMap .croptonBox").hide();
    });

    $("#homeMap .delamereforest a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .delamereforestBox").toggle();
    });
    $("#homeMap .delamereforestBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .delamereforestBox").show();
    }, function () {
        $("#homeMap .delamereforestBox").hide();
    });

	$("#homeMap .deerpark a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .deerparkBox").toggle();
    });
    $("#homeMap .deerparkBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .deerparkBox").show();
    }, function () {
        $("#homeMap .deerparkBox").hide();
    });

    $("#homeMap .forestofdean a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .forestofdeanBox").toggle();
    });
    $("#homeMap .forestofdeanBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .forestofdeanBox").show();
    }, function () {
        $("#homeMap .forestofdeanBox").hide();
    });

    $("#homeMap .keldy a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .keldyBox").toggle();
    });
    $("#homeMap .keldyBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .keldyBox").show();
    }, function () {
        $("#homeMap .keldyBox").hide();
    });

    $("#homeMap .sherwoodforest a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .sherwoodforestBox").toggle();
    });
    $("#homeMap .sherwoodforestBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .sherwoodforestBox").show();
    }, function () {
        $("#homeMap .sherwoodforestBox").hide();
    });

    $("#homeMap .strathyre a").hover(function (e) {
        e.preventDefault();
        $("#homeMap .strathyreBox").toggle();
    });

    $("#homeMap .strathyreBox").hover(function (e) {
        e.preventDefault();
        $("#homeMap .strathyreBox").show();
    }, function () {
        $("#homeMap .strathyreBox").hide();
    });   
			
			
			
		},  
		unmatch : function() {
			
			$("#homeMap .flag").each(function() {
				$(this).unbind('mouseenter mouseleave');
			});

			$("#homeMap .flag a").each(function() {
				$(this).unbind('mouseenter mouseleave');
			});

		}
	});
		   
});