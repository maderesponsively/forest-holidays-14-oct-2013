// rCarousel - see http://ryrych.github.com/rcarousel/

/*
jQuery(document).ready(function ($) {
    // navigation bullet fading
    $(".bullet").hover(function() {
        $(this).css("opacity", 0.7);
    }, function() {
        $(this).css("opacity", 1.0);
    });

    // both buttons to semi-transparency on wrapper element hover
    $(".wrap-carousel").each(function() {
        var prev = $(this).find(".carousel-prev");
        var next = $(this).find(".carousel-next");
        
        $(this).hover(function() {
            next.css("opacity", 0.7);
            prev.css("opacity", 0.7);
        }, function() {
            next.css("opacity", 0);
            prev.css("opacity", 0);
        });
        
        prev.hover(function() {
            prev.css("opacity", 1);
            next.css("opacity", 0.7);
        }, function() {
            prev.css("opacity", 0.7);
        });
        next.hover(function() {
            prev.css("opacity", 0.7);
            next.css("opacity", 1);
        }, function() {
            next.css("opacity", 0.7);
        });
    });

    // individual buttons to full colour on button hover
    
});
/*
function initializeCarousel(event, data) {
    var carouselWrapper = $(event.target).parent();
    
    //Pagination
    var _total, i, _link;
    _total = $(event.target).rcarousel("getTotalPages");

    var pages = $(carouselWrapper).find(".pages");
    for (i = 0; i < _total; i++) {
        _link = $("<a href='#'></a>");
        
        $(_link).bind("click", {
            page: i
        }, function(clickEvent) {
            $(event.target).rcarousel("goToPage", clickEvent.data.page);
            clickEvent.preventDefault();
        }).addClass("bullet off").appendTo(pages);
    }
    
    //Next and Prev... Needs to be modular
    var prev = $(carouselWrapper).find(".carousel-prev");
    var next = $(carouselWrapper).find(".carousel-next");
    
    $(prev).bind("click", function(clickEvent) {
        $(event.target).rcarousel("prev");
        clickEvent.preventDefault();
    });
    
    $(next).bind("click", function(clickEvent) {
        $(event.target).rcarousel("next");
        clickEvent.preventDefault();
    });

    // mark first page as active
    $("a:eq(0)", pages).removeClass("off").addClass("on").css("background-image", "url(/img/slide-pip-on.png)");
}
*/
function pageLoaded(event, data) {
    var pages = $(event.target).parent().find(".pages");
    
    $("a.on", pages).removeClass("on").css("background-image", "url(/img/slide-pip-off.png)");

    $("a", pages).eq(data.page).addClass("on").css("background-image", "url(/img/slide-pip-on.png)");
}


/*Flexslider */
/*
$(window).load(function() {
	$('#slider .box .wrap #hero').flexslider({
		animation: "slide",
		selector: ".carousel > li",
		pausePlay: false,
		controlNav: true,
		slideshowSpeed: 7000
	});
	
	$('.testimonials .wrap-carousel-text').each(function() {	
		$(this).flexslider({
			animation: "slide",
			selector: ".carousel > li",
			pausePlay: false,
			controlNav: false,
			slideshowSpeed: 7000,
			smoothHeight: false
		});
	});

	$('.small-carousel').each(function() {	
		$(this).flexslider({
			animation: "slide",
			selector: ".carousel > li",
			pausePlay: false,
			controlNav: true,
			slideshowSpeed: 7000
		});
	});
	
});
*/

/* main carousel settings, controlling transition speed etc */
function carouselSetup() {
	
	$('#slider .box .wrap #hero').flexslider({
		animation: "slide",
		selector: ".carousel > li",
		pausePlay: false,
		controlNav: true,
		slideshowSpeed: 7000
	});
	
	$('.testimonials .wrap-carousel-text').each(function() {	
		$(this).flexslider({
			animation: "slide",
			selector: ".carousel > li",
			pausePlay: false,
			controlNav: false,
			slideshowSpeed: 7000
		});
	});

	$('.small-carousel').each(function() {	
		$(this).flexslider({
			animation: "slide",
			selector: ".carousel > li",
			pausePlay: false,
			controlNav: true,
			slideshowSpeed: 7000
		});
	});
	
	
	/*
    $("#hero .carousel").rcarousel({
        visible: 1,
        step: 1,
        speed: 700,
        auto: {
            enabled: autoScroll
        },
        width: 1340,
        height: 515,
        start: initializeCarousel,
        pageLoaded: pageLoaded
    });
	
    
    $("#carousel1 .carousel").rcarousel({
        visible: 1,
        step: 1,
        speed: 500,
        auto: {
            enabled: autoScroll
        },
        width: 470,
        height: 325,
        start: initializeCarousel,
        pageLoaded: pageLoaded
    });
    
    $("#carousel2 .carousel").rcarousel({
        visible: 1,
        step: 1,
        speed: 500,
        auto: {
            enabled: autoScroll
        },
        width: 470,
        height: 325,
        start: initializeCarousel,
        pageLoaded: pageLoaded
    });
 
    $("#carousel-testimonial .carousel").rcarousel({
        visible: 1,
        step: 1,
        speed: 500,
        width: 270,
        height: 70,
        start: initializeCarousel,
        pageLoaded: pageLoaded
    });*/
}
// end rCarousel