jQuery(document).ready(function ($) {
 
	
	$(".fancybox.iframe").fancybox({
			type: 'iframe',
			padding: [30, 0, 30, 0],
        	autoSize : false,
			iframe : {
        		preload : false
    		},
			helpers : {
				title : {
					type: 'inside'
				}
        	},
        	beforeLoad : function() {                    
        		this.width = parseInt(this.href.match(/width=[0-9]+/i)[0].replace('width=',''));  
        		//this.height = parseInt(this.href.match(/height=[0-9]+/i)[0].replace('height=',''));
        	}		
	});

	
	/*
	
	$(".fancybox.galleryImg").fancybox({
			type: 'image',
			padding: [30, 100, 30, 100],
			openEffect	: 'none',
			closeEffect	: 'none',
			nextEffect : 'none',
			prevEffect : 'none',
			helpers : {
				title : {
					type: 'inside'
					
				}
        	},
			afterShow: function() {
            	$(".fancybox-wrap").touchwipe({
					 wipeLeft: function() { $.fancybox.prev( 'left' ); },
					 wipeRight: function() { $.fancybox.next( 'right' ); },
					 wipeUp: function() { $.fancybox.prev( 'left' ); },
					 wipeDown: function() { $.fancybox.next('right' ); },
					 min_move_x: 20,
					 min_move_y: 20,
					 preventDefaultEvents: true
				});
        	}
	});
	
	*/	
	
	$(".fancybox.img").fancybox({
			type: 'image',
			padding: [30, 100, 30, 100],
			openEffect	: 'none',
			closeEffect	: 'none',
			nextEffect : 'none',
			prevEffect : 'none',
			helpers : {
				title : {
					type: 'inside'
				},
				thumbs : {
						width: 50,
						height: 37
				}
				//buttons	: {}
        	},
			beforeShow : function() {
				this.title = '<div class="fan-nav"><a title="Prev" href="javascript:;" onclick="$.fancybox.prev();" class="prev"><span>PREV</span></a>\
				<span class="count">' + (this.index + 1) + ' / ' + this.group.length + '</span>\
				<a title="Next" href="javascript:;" onclick="$.fancybox.next();" class="next"><span>NEXT</span></a></div>\
				' + (this.title ? '<div class="fan-title">' + this.title + '</div>' : '');
			},
			afterShow: function() {
            	$(".fancybox-wrap").touchwipe({
					 wipeLeft: function() { $.fancybox.prev( 'left' ); },
					 wipeRight: function() { $.fancybox.next( 'right' ); },
					 wipeUp: function() { $.fancybox.prev( 'left' ); },
					 wipeDown: function() { $.fancybox.next('right' ); },
					 min_move_x: 20,
					 min_move_y: 20,
					 preventDefaultEvents: true
				});
				
			}
	});
	
	
	
    
    //$(".gallery .item a").fancybox({
    //animationSpeed: 'slow',
    //theme: 'forest',
   // deeplinking: false,
    //autoplay_slideshow: true,
    /* light_rounded / dark_rounded / light_square / dark_square / facebook */
    //slideshow: 6000
    /*social_tools: '<div class="pp_social"><div class="twitter"><a href="http:twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http:platform.twitter.com/widgets.js"></script></div><div class="facebook"><iframe src="http:www.facebook.com/plugins/like.php?locale=en_US&href=' + location.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:23px;" allowTransparency="true"></iframe></div></div>'*/ /* html or false to disable */
   //});

    //$("#specificCabinSiteMap a").fancybox({
        //animationSpeed: 'slow',
        //theme: 'forest',
        /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        //slideshow: 6000
    //});


});