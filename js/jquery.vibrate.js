jQuery.fn.vibrate = function (conf) {
    var config = jQuery.extend({
        speed:        30, 
        duration:    2000, 
        frequency:    5000, 
        spread:        3
    }, conf);

    return this.each(function () {
        var t = jQuery(this);
        var timeout = null;

        var vibrate = function () {
            var topPos    = Math.floor(Math.random() * config.spread) - ((config.spread - 1) / 2);
            var leftPos    = Math.floor(Math.random() * config.spread) - ((config.spread - 1) / 2);
            var rotate    = Math.floor(Math.random() * config.spread) - ((config.spread - 1) / 2);

            t.css({
                position:            'relative', 
                left:                leftPos + 'px', 
                top:                topPos + 'px', 
                WebkitTransform:    'rotate(' + rotate + 'deg)'  // cheers to erik@birdy.nu for the rotation-idea
            });
        };

        var doVibration = function () {
            var vibrationInterval = setInterval(vibrate, config.speed);

            var stopVibration = function () {
                clearInterval(vibrationInterval);
                t.css({
                    position:            'static', 
                    WebkitTransform:    'rotate(0deg)'
                });
            };

            timeout = setTimeout(stopVibration, config.duration);
        };
        
        var clearVibration = function () {
            clearTimeout(timeout);
        };

        setInterval(doVibration, config.frequency);
    });
};