/* Responsive Code */
/* ----------------------------------------------- */
Modernizr.load([
  {
	  test: window.matchMedia,
	  nope: '/js/media.match.min.js'
  },

  // Load enquire & enquire-specific code
  '/js/enquire.js',
  '/js/mediaqueries.js'

]);





/* End of response code 
-------------------------------------------------*/

function cssObject(position) {
    var cssObj = {
        'top': position.top - 250,
        'left': position.left - 45
    };
    return cssObj;
}

// Handles form field hinting
function hinting($hints) {
    $hints.each(function () {
        if ($(this).val() === '') {
            $(this).val($(this).attr('title'));
            $(this).addClass('hinted'); // doesn't seem to work reliably chained
        }
        $(this).focus(function () {
            if ($(this).val() === '' || $(this).val() == $(this).attr('title')) {
                $(this).val('').removeClass('hinted');
            }
        });
        $(this).blur(function () {
            if ($(this).val() === '') {
                $(this).val($(this).attr('title'));
                $(this).addClass('hinted'); // doesn't seem to work reliably chained
            }
        });
    });
}

jQuery(function () {
    var $hints = $(':input.hint');
    if ($hints.length > 0) {
        hinting($hints);
    }
});

function BindEvents() {
    var postcode = $("#journeyPlanner fieldset #postcode").val();
    if (postcode !== "") {
        $("#journeyPlanner .results").slideDown('fast');
    }
};

jQuery(document).ready(function ($) {

    $("#journeyPlanner fieldset .goBtn").click(function (e) {
        //e.preventDefault();

        // Run a check to see if content exists first
        //var postcode = $("#journeyPlanner fieldset #postcode").val();
        //if (postcode !== "") {
        //    $("#journeyPlanner .results").slideDown('fast');
        //}
    });




    // Search Form Functions - need to go before prettyPhoto
    //searchForm();

    //$("#main #right a").fancybox({

	
    //    animationSpeed: 'slow',
    //    theme: 'forest',
    //    deeplinking: false,
        /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        //slideshow: 6000
    //});
	
	

    //$("#thumbnails .item a[rel^='prettyPhoto']").fancybox({


    //    animationSpeed: 'slow',
    //    theme: 'forest',
    //    deeplinking: false,
        /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        //slideshow: 6000
    //});
	

    //Seasons Carousel - Autoplay...
    var delay = 4000;
    var autoplayTimeout = setTimeout(seasonsCarouselAutoplay, delay);

    function seasonsCarouselAutoplay() {
        var seasonSlides = $(".wrap-carousel #seasons ul.carousel");

        var currentSlide = $(seasonSlides).children("li:visible");

        var currentSlideIndex = $(currentSlide).index();
        var nextSlideIndex = currentSlideIndex + 1;

        var pages = $(".wrap-carousel #seasons .pages");
        $(pages).find("a").removeClass("active");

        //If there is a next slide (not on end slide)...
        if ($(seasonSlides).children("li").get(nextSlideIndex)) {
            var nextIcon = $(pages).children("a").get(nextSlideIndex);
            $(nextIcon).addClass("active");

            var nextSlide = $(seasonSlides).children("li").get(nextSlideIndex);

            $(currentSlide).fadeOut(500);
            $(nextSlide).fadeIn(500);
        } else {
            var nextIcon = $(pages).children("a").get(0);
            $(nextIcon).addClass("active");

            var nextSlide = $(seasonSlides).children("li").get(0);

            //Crossfade
            $(currentSlide).fadeOut(500);
            $(nextSlide).fadeIn(500);
        }

        //Looping timeout...
        autoplayTimeout = setTimeout(seasonsCarouselAutoplay, delay);
    }

    //Seasons Carousel - Click events...
    $(".wrap-carousel #seasons .pages a").click(function (e) {
        clearTimeout(autoplayTimeout);

        if (!$(this).hasClass("active")) {
            var index = $(this).index();

            var seasonSlides = $(this).parents("#seasons").children("ul.carousel");

            var targetSlide = $(seasonSlides).children("li").get(index);
            var currentSlide = $(seasonSlides).children("li:visible");

            //Clearing queue... just in case someone decides to click too quickly
            $(currentSlide).clearQueue();
            $(currentSlide).stop();
            $(targetSlide).clearQueue();
            $(targetSlide).stop();

            $(this).parent().children("a").removeClass("active");
            $(this).addClass("active");

            //Crossfade
            $(currentSlide).fadeOut(500);
            $(targetSlide).fadeIn(500);
        }

        e.preventDefault();
    });

    //    $(".rating a").click(function (event) {
    //        var rating = $(this).data("rating");
    //        //console.log("Rated - "+rating);

    //        /*$.ajax({
    //        type: "POST",
    //        url: "/path/to/vote/handler.php",
    //        data: "item_id=12345&vote="+amnt,
    //        dataType: "json",
    //        success: function(res){
    //        $('.star-rating').width(res.width);
    //        $('#current-rating-result').html(res.status);
    //        }
    //        });*/

    //        var width = rating * 17; //17 = Icon width
    //        //console.log($(this).parents('.star-rating').find('.current-rating'));
    //        $(this).parents('.rating').find('.current-rating').width(width); //Remove when AJAX in place...

    //        $(this).parents('.rating').find('.current-rating-result').html('<img src="/img/icon-rating-completed.png" alt="Done" />'); //Remove when AJAX in place...

    //        event.preventDefault();
    //    });

    //    $(".rating").each(function () {
    //        var rating = 1; //Demo... remove when AJAX in place...

    //        /*$.ajax({
    //        type: "GET",
    //        url: "/path/to/vote/handler.php",
    //        data: "item_id=12345&vote="+amnt,
    //        dataType: "json",
    //        success: function(res){*/
    //        var width = rating * 17; //17 = star icon width

    //        //$(this).find('.current-rating').width(width);
    //        //$(this).find('.current-rating-result').html('<img src="/img/icon-rating-completed.png" alt="Done" />');
    //        /*}
    //        });*/
    //    });

    //Un-obtrusive... ensures that accessibility users get to see everything.
	//
	
	var allPanels = $(".tabbed .mobileTabs").next();
	
	$(".tabbed .mobileTabs").tap(50, function() {  
		var hidCont = $(this).next();
		//var flexSize = $('.flex-viewpoint').width();
		
		$(".tabbed a").removeClass("active");
		  
		if (hidCont.is(":hidden")) {		  
			allPanels.hide().slideUp();
			$(this).next().slideDown(function() {
				$(".small-carousel").resize();
				$("html, body").animate({
					 scrollTop: $(".layoutContainer.tabbed").offset().top - 50
				}, 300);
			});
			$(this).addClass("active");
			return false;
		}
		else if (hidCont.is(":visible")) {
				hidCont.slideUp();
		}
	});
	




    //Catching menu pill click events... update hatchet location and .active class
    //I must apologise for the mixed use of single and double quotations. Force of habit to use single, but tried to stick with double... resulted in a mix which I don't have time to change and test etc
    $(".menu-pills a").click(function (event) {
		
        var target = $(this).attr('href');

		$(".tabbed").children().hide();
		$(".tabbed").children(target).show();
				
		$(this).parents('.menu-pills').find('a').removeClass('active');
		$(this).addClass('active');
		
		var width = $(this).css('width').substr(0, $(this).css('width').length - 2);
		var hatchetLeft = $(this).parent().position().left + ((width / 2) - 10);
		
		$('.menu-hatchet').clearQueue();
		$('.menu-hatchet').stop();
		$('.menu-hatchet').animate({ left: hatchetLeft }, 'slow');
		
		event.preventDefault();

    });
		
		
	// Responsive tabs 
	
	

		
		



	


    //Basket
    $('.add-to-basket').click(function (event) {
        //Increment item count...
        //var currentCount = $('#basket #basket-count').html();
        //currentCount++;
        //$('#basket #basket-count').html(currentCount);

        //Shake basket...
        //var speed = 100;
        //var duration = 400;
        //var interval = setInterval(shakeBasket, speed);

        //Stop shaking after duration...
        //        setTimeout(function () {
        //            clearInterval(interval);

        //            $('#basket-icon').css({
        //                position: 'relative',
        //                left: '0px',
        //                top: '0px',
        //                WebkitTransform: 'rotate(0deg)'  // cheers to erik@birdy.nu for the rotation-idea
        //            });
        //        }, duration);


        //Tree state... going by number of items in basket
        //        var state1 = 0;
        //        var state2 = 2;
        //        var state3 = 4;
        //        var state4 = 6;

        //        if (currentCount >= state1 && currentCount < state2) {
        //            $('#basket-tree img').attr('src', '/img/tree-state-1.png');
        //        } else if (currentCount >= state2 && currentCount < state3) {
        //            $('#basket-tree img').attr('src', '/img/tree-state-2.png');
        //        } else if (currentCount >= state3 && currentCount < state4) {
        //            $('#basket-tree img').attr('src', '/img/tree-state-3.png');
        //        } else if (currentCount >= state4) {
        //            $('#basket-tree img').attr('src', '/img/tree-state-4.png');
        //        }

        //Increase cost
        //var currentCost = $('#basket-cost').html();
        //currentCost = parseInt(currentCost) + 500;
        //$('#basket-cost').html(currentCost);

        //event.preventDefault();
    });

//    function shakeBasket() {
//        var spread = 5;
//        var shake = 0;

//        var topPos = Math.floor(Math.random() * spread) - ((spread - 1) / 2);
//        var leftPos = Math.floor(Math.random() * spread) - ((spread - 1) / 2);
//        var rotate = Math.floor(Math.random() * spread) - ((spread - 1) / 2);

//        $('#basket-icon').css({
//            position: 'relative',
//            left: leftPos + 'px',
//            top: topPos + 'px',
//            WebkitTransform: 'rotate(' + rotate + 'deg)'  // cheers to erik@birdy.nu for the rotation-idea
//        });

//        shake++;
//    }


});

function giftAidInitialize() {
    $(".wtOptionsBody input").focus(function () {
        giftAidExpand($(".wtGiftAid"));
    });
    $(".wtOptionsBody input:radio ").change(function () {
        $(".wtOptionsBody input:text").val('');
        giftAidExpand($(".wtGiftAid"));
    });
    $(".wtOptionsBody input:text").click(function () {
        $(".wtOptionsBody input:radio").removeAttr("checked");
        giftAidExpand($(".wtGiftAid"));
    });

}

function giftAidExpand(o) {
    if (o.css("display") == "none") {
        o.slideDown("slow");
    }
}

// Shows/hides the Benefits of Registering info box
function benefitsBoxDisplay() {
    $(document).click(function () {
        if ($("#footerInner .infoBox").is(':visible')) {
            $("#footerInner .infoBox").hide();
        }
    });

    $("#footerInner .benefitsLink").click(function (e) {
        e.stopPropagation();
    });

    $("#footerInner .infoBox").click(function (e) {
        e.stopPropagation();
    });

    
    // open when the info link is clicked
    $("#footerInner .benefitsLink").click(function (e) {
        e.preventDefault();
        $("#footerInner .infoBox").show();
    });

    // close when the info box close button is clicked
    $("#footerInner .infoBox .greenButton").click(function (e) {
        e.preventDefault();
        $("#footerInner .infoBox").hide();
    });
}

// Shows/hides the Search Detail (In-Cabin Entertainment) info box
function SearchDetailBoxDisplay() {
    $(document).click(function () {
        if ($("#contentBlockContainer .infoBox").is(':visible')) {
            $("#contentBlockContainer .infoBox").hide();
        }
    });

    $("#contentBlockContainer .bundleInfoLink").click(function (e) {
        e.stopPropagation();
    });

    $("#contentBlockContainer .infoBox").click(function (e) {
        e.stopPropagation();
    });


    // open when the info link is clicked
    $("#contentBlockContainer .bundleInfoLink").click(function (e) {
        e.preventDefault();
        $("#contentBlockContainer .infoBox").show();
    });

    // close when the info box close button is clicked
    $("#contentBlockContainer .infoBox .greenButton").click(function (e) {
        e.preventDefault();
        $("#contentBlockContainer .infoBox").hide();
    });
}

function locationTabInitialize() {
    // hide the second tab content by default
    //alert('Initialize start');
    $(".testTabs .tContent:eq(1)").hide();
    //alert('...hidden');
    $locationTabLi = $(".testTabs").find('li.item');
    //alert('$locationTabLi set');
    $locationTabLink = $("p.tab a", $locationTabLi);
    //alert('$locationTabLink set');
    $locationTabDiv = $(".tContent", $locationTabLi);
    //alert('$locationTabDiv set');

    $locationTabLink.click(function (e) {
        locationTabSwitch(this);
        e.preventDefault();
    });
}

// Switches the Search Detail Location page tabs
function locationTabSwitch(o) {
    var cl = $locationTabLink.index(o);
    //var ot = (cl === 0) ? 4 : (cl === 4) ? 0 : 1;
    var ot = (cl === 0) ? 1 : 0;
    //alert('Clicked: ' + cl + ' Other: ' + ot);
    // switch button on states
    $($($locationTabLi)[cl]).addClass('active');
    $($($locationTabLi)[ot]).removeClass('active');
    
    // switch tab content shown
    $($($locationTabDiv)[cl]).fadeIn('slow');
    $($($locationTabDiv)[ot]).hide();
}

/* Additional code for responsive - Author David Smith 
-------------------------------------------------------
-------------------------------------------------------*/

$(function() {
	/* Responsive Main Menu
	--------------------------------------------- */
	var hidCont = $('header nav');

	$("#menuTrigger").tap(50, function() {
		if (hidCont.is(":hidden")) {
				hidCont.slideDown();
				$(this).removeClass('closed');	
		}
		else if (hidCont.is(":visible")) {
				hidCont.slideUp();  
				$(this).addClass('closed');
		}
	});
	
	/* Responsive Customer Menu
	--------------------------------------------- */
	var userMenuCont = $('#userMenu');

	$(".customerMenuTrigger").tap(50, function() {
		

		if (userMenuCont.is(":hidden")) {
				userMenuCont.slideDown();	
		}
		else if (userMenuCont.is(":visible")) {
			
				$('a').bind("click.myDisable", function() { return false; });
				
				userMenuCont.slideUp(function(){
					
				$('a').unbind("click.myDisable");	
					
				});  
		}
	});
	
});
